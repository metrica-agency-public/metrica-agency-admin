import { FunctionComponent, SVGProps } from 'react';

export type UUID = string;

export interface WebData {
  tempAccessToken: string;
  accessToken: string;
  refreshToken: string;
  role: RoleType;
  userId: UUID;
}

export enum RequestStatus {
  IDLE,
  LOADING,
  COMPLETE,
  ERROR,
}

export enum RoleType {
  UNAUTHORIZED = 'UNAUTHORIZED', // Эта роль только на фронте
  USER_TEMP = 'USER_TEMP',
  USER = 'USER',
  ADMIN = 'ADMIN',
  SA = 'SA',
}

// TODO : ADD ALL RESPONSE PARSE
export interface UserInfo {
  firstName: string;
  lastName: string;
}

export enum GiftStatusEnum {
  DEFAULT = 'DEFAULT',
  CREATED = 'CREATED',
  PAID = 'PAID',
  VIEWED = 'VIEWED',
  ACCEPTED = 'ACCEPTED',
  REJECTED = 'REJECTED',
}

export enum EnvelopePriceType {
  FOREVER = 'FOREVER',
  ONE_TIME = 'ONE_TIME',
}

export interface GiftPayment {
  priceEnvelope: number;
  priceEnvelopeType: EnvelopePriceType;
  priceContent: number;
  priceFinal: number;
  giftStatus: GiftStatusEnum;
}

export interface GiftReview {
  rating: number;
  text: string;
}

export interface GiftContent {
  amountMoney: number;
  message: string;
  signature: string;
}

export interface GiftStatus {
  createdAt: string;
  endedAt: string;
  status: GiftStatusEnum;
}

export interface Gift {
  id: UUID;
  createdAt: string;
  giftContent: GiftContent;
  giftPayment: GiftPayment;
  review: GiftReview;
  statusList: GiftStatus[];
  userList: UserInfo[];
}

export type Icon = FunctionComponent<SVGProps<SVGSVGElement>>;

export type Tag = {
  id: UUID;
  title: string;
  description: string;
  emoji?: string;

  isSelected?: boolean;
};
