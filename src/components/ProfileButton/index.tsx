import ProfileSVG from 'assets/svg/profile.svg';
import { observer } from 'mobx-react-lite';
import appStore from 'stores/app-store';
import styles from './styles.module.scss';

export const ProfileButton = observer(() => {
  const onClickHandler = () => {
    appStore.logout();
  };

  return (
    <div className={styles.profile} onClick={onClickHandler}>
      <p className={styles.login}>Выйти</p>
      <img src={ProfileSVG} alt={'Профиль'} />
    </div>
  );
});
