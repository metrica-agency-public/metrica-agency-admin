import { Upload } from "antd";
import { UploadChangeParam } from "antd/lib/upload";
import { UploadFile } from "antd/lib/upload/interface";
import { ResUploadFile } from "api/media/classes";
import { useEffect, useState } from "react";
import appStore from "stores/app-store";
import { Constants } from "utils/constants";

interface Props {
  value?: ResUploadFile;
  onChange?: (resFile?: ResUploadFile) => void;
}

export const UploadImage = (props: Props) => {
  const [fileList, setFileList] = useState<UploadFile<ResUploadFile>[]>([]);

  useEffect(() => {
    if (props.value) {
      setFileList([
        {
          uid: props.value.id,
          name: props.value.originalName,
          status: "done",
          url: props.value.url,
        },
      ]);
    }
  }, [props.value]);

  const handleChange = (info: UploadChangeParam<UploadFile<ResUploadFile>>) => {
    console.log("FileUpload_handleChange =", info);

    setFileList(info.fileList);

    if (info.file.status === "removed") {
      if (props.onChange) {
        props.onChange(undefined);
      }
      return;
    }

    if (info.file.status === "done") {
      if (info.file.response) {
        if (props.onChange) {
          props.onChange(info.file.response);
        }
      }
    }
  };

  return (
    <Upload
      name="file"
      listType="picture-card"
      action={Constants.API_V1_URL + "/sa/media"}
      headers={{
        [appStore.X_ACCESS_TOKEN_TITLE]: appStore.token(),
      }}
      fileList={fileList}
      onChange={handleChange}
    >
      {fileList.length === 0 && "+ Upload"}
    </Upload>
  );
};
