import React from "react";
import styles from "./Logo.module.scss";
import LogoImg from "assets/logo.png";
import { Image } from "components/Image/Image";
import { NavLink } from "react-router-dom";

export const Logo = (props: { className?: string }) => {
  const { className } = props;

  return (
    <NavLink to={"/"} className={className}>
      <Image className={ styles.logo }>
        <img src={ LogoImg }
             alt={ "ekonvert" }/>
      </Image>
    </NavLink>
  );
}


