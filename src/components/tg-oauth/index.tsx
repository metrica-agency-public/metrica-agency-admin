import { useEffect } from "react";

export const TGOauth = () => {
  useEffect(() => {
    const script = document.createElement("script");

    script.src = "https://telegram.org/js/telegram-widget.js?19";
    script.setAttribute("data-telegram-login", "metrica_auth_bot");
    script.setAttribute("data-size", "medium");
    script.setAttribute("data-auth-url", "https://api.metrica-agency.ru/api/v1/claims/oauth");
    script.setAttribute("data-request-access", "write");

    script.async = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return <div></div>;
};
