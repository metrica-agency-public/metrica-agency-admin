import { useLayoutEffect } from "react";

interface Props {
  /** Назначение платежа */
  description: string;

  /** Сумма платежа */
  amount: number;

  /** номер заказа  (необязательно) */
  invoiceId: string;

  /** идентификатор плательщика (необязательно) */
  accountId: string;

  escrow?: any;

  /** Опциональные параметры */
  data?: any;
}

type PaymentWidget = {
  pay: (
    /** Тип транзакции */
    type: "auth" | "charge",
    paymentParams: PaymentParams,
    callBack: {
      /**  success, действие при успешной оплате */
      onSuccess: (options: any) => any;
      /** fail, действие при неуспешной оплате*/
      onFail: (reason: any, options: any) => any;
      /** Вызывается как только виджет получает от api.cloudpayments ответ с результатом транзакции */
      onComplete: (paymentResult: any, options: any) => any;
    }
  ) => Promise<any>;
};

type PaymentParams = {
  /** id из личного кабинета */
  publicId: string;

  /** Назначение платежа */
  description: string;

  /** Сумма платежа */
  amount: number;

  /** Валюта платежа */
  currency: "RUB";

  /** идентификатор плательщика (необязательно) */
  accountId: string;

  /** номер заказа  (необязательно) */
  invoiceId: string;

  /** email плательщика (необязательно) */
  email?: string;

  /** дизайн виджета (необязательно) */
  skin?: "mini";

  /** Опциональные параметры */
  data?: any;

  escrow?: any;
};

const PaymentButton = (props: Props) => {
  const publicId = "pk_dac4a022248f0d995b212a7946a29";
  const { amount, accountId, invoiceId, description, data, escrow } = props;
  const pay = () => {
    const cloudPaymentsWidget: any = (window as any)?.cp;
    if (cloudPaymentsWidget && publicId) {
      const widget: PaymentWidget = new cloudPaymentsWidget.CloudPayments();

      console.log("AAA =", escrow);

      const paymentParams: PaymentParams = {
        publicId,
        description,
        amount,
        currency: "RUB",
        accountId,
        invoiceId,
        skin: "mini",
        data,
        escrow,
      };

      console.log("KKK =", paymentParams);

      widget.pay("charge", paymentParams, {
        onSuccess: (options: any) => {
          console.log("OnSuccess =", options);
        },
        onFail: (reason: any, options: any) => {
          console.log("onFail reason =", reason);
          console.log("onFail options =", options);
        },
        onComplete: (paymentResult: any, options: any) => {
          console.log("onComplete result =", paymentResult);
          console.log("onComplete options =", options);
        },
      });
    } else {
      console.error("Не получилось загрузить виджет CloudPayments", {
        props,
      });
    }
  };
  useLayoutEffect(() => {
    const script = document.createElement("script");
    script.src = "https://widget.cloudpayments.ru/bundles/cloudpayments.js";
    document.head.appendChild(script);
    return () => {
      document.head.removeChild(script);
    };
  }, []);

  return <button onClick={() => pay()}>Тестовая оплата</button>;
};
export default PaymentButton;
