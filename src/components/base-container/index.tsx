import cn from "classnames";
import { NavBlock } from "components/nav-block";
import { ProfileButton } from "components/ProfileButton";
import { observer } from "mobx-react-lite";
import React, { ReactNode } from "react";
import styles from "./styles.module.scss";

type Props = {
  children?: ReactNode;
  className?: string;
};

export const BaseContainer = observer((props: Props) => {
  const { children, className } = props;

  return (
    <div className={cn(styles.root, className)}>
      <header className={styles.header}>
        Админ панель: METRICA
        <ProfileButton />
      </header>
      <div className={styles.container}>
        {<NavBlock />}
        <div className={styles.content}>{children}</div>
      </div>
    </div>
  );
});
