import React, { Dispatch, HTMLProps, SetStateAction } from "react"
import InputMask from 'react-input-mask'
import cn from "classnames"

import { Icon } from 'types/icon'
import { ReactComponent as WarningIcon } from "assets/svg/warning.svg"

import styles from "./styles.module.scss"

export const CODE_6_REGEX = new RegExp('^([0-9]{3})-?([0-9]{3})$')
const PRICE_SYMBOLS_REGEX = new RegExp('^[0-9]*$')

export enum InputType {
  TEXT,
  PRICE,
  CODE,
  AUTH,
  CARD,
}

export interface InputErrorHinter {
  isError?: boolean
  /* hint
  label with text under input
  depends on 'isError'
  **/
  errorHint?: string
  defaultHint?: string
}

export interface InputDispatcher {
  setValue: Dispatch<SetStateAction<string>>
}

interface InputProps extends React.ComponentPropsWithoutRef<'input'>, InputErrorHinter, InputDispatcher {
  inputType: InputType
  inputIcon?: Icon // Icon fixed to left, top & bottom borders
  label?: string
  containerClassName?: string

  inputContentLeftIcon?: Icon
  inputContentRightIcon?: Icon
}

export const Input = (props: InputProps) => {
  const {
    inputType,
    label,
    inputIcon: InputIcon,
    containerClassName,

    setValue,

    inputContentLeftIcon: ContentLeftIcon,
    inputContentRightIcon: ContentRightIcon = WarningIcon,

    isError = false,
    errorHint,
    defaultHint,

    ...otherProps
  } = props

  return (
    <div className={ cn(
      styles.container,
      containerClassName,
    ) }>
      {
        label &&
				<label className={ styles.label }>
          { label }
				</label>
      }

      <div className={ cn(
        styles.inputContainer,
        isError && styles.inputContainer_Error,
        inputType === InputType.TEXT && styles.inputContainer_Text,
        inputType === InputType.PRICE && styles.inputContainer_Price,
        inputType === InputType.CODE && styles.inputContainer_Code,
        inputType === InputType.AUTH && styles.inputContainer_Auth,
        inputType === InputType.CARD && styles.inputContainer_Card,
      ) }>
        {
          InputIcon &&
          inputType === InputType.PRICE &&
					<div className={ styles.inputContainerIcon }>
						<InputIcon className={ styles.inputIcon }/>
					</div>
        }

        <div className={ styles.inputContent }>
          {
            ContentLeftIcon && <ContentLeftIcon className={ styles.inputContentLeftIcon }/>
          }
          {
            (inputType === InputType.TEXT &&
							<TextInput className={ styles.inputInstance }
												 setValue={ setValue }
                         { ...otherProps }/>) ||

            (inputType === InputType.PRICE &&
							<PriceInput className={ styles.inputInstance }
													setValue={ setValue }
                          { ...otherProps }/>) ||

            (inputType === InputType.CODE &&
							<CodeInput className={ styles.inputInstance }
												 setValue={ setValue }
                         { ...otherProps }/>) ||

            (inputType === InputType.AUTH &&
							<AuthInput className={ styles.inputInstance }
												 setValue={ setValue }
                         { ...otherProps }/>) ||

            (inputType === InputType.CARD &&
							<CardInput className={ styles.inputInstance }
												 setValue={ setValue }
                         { ...otherProps }/>)
          }


          {
            ContentRightIcon &&
            isError &&
            inputType === InputType.TEXT &&
						<ContentRightIcon className={ styles.inputContentRightIcon }/>
          }
        </div>
      </div>

      {
        (errorHint !== undefined || defaultHint !== undefined) &&
				<span className={ cn(
          styles.hint,
          errorHint && isError && styles.hintError,
          ((isError && errorHint) || (!isError && defaultHint)) && styles.hintActive,
        ) }>
            { errorHint || defaultHint }
				  </span>
      }
    </div>
  )
}

interface InputInstanceProps extends HTMLProps<HTMLInputElement>, InputDispatcher {
}

const PriceInput = (props: InputInstanceProps) => {
  const {
    setValue,
    ...otherProps
  } = props

  const blockInvalidChar = (e: React.KeyboardEvent<HTMLInputElement>) => ['e', 'E', '+', '-'].includes(e.key) && e.preventDefault();

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e.target.value

    if ((e.target.validity.valid && PRICE_SYMBOLS_REGEX.exec(val)) || val === '') {
      setValue(val)
    }
  }

  return (
    <input onChange={ onChange }
           onWheel={ (e: React.WheelEvent<HTMLInputElement>) => e.currentTarget.blur() }
           onKeyDown={ blockInvalidChar }
           min={ 0 }
           { ...otherProps }/>
  )
}


const AuthInput = (props: InputInstanceProps) => {
  const {
    setValue,
    value,
    ...otherProps
  } = props

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value)
  }

  return (
    <InputMask mask={ '+7(999)999-99-99' }
               onChange={ onChange }
               value={ value }>
      <input type={'tel'} { ...otherProps }/>
    </InputMask>
  )
}

const TextInput = (props: InputInstanceProps) => {
  const {
    setValue,

    ...otherProps
  } = props
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value)
  }

  return (
    <input onChange={ onChange }
           { ...otherProps }/>
  )
}


const CardInput = (props: InputInstanceProps) => {
  const {
    setValue,
    value,
    ...otherProps
  } = props

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value)
  }

  return (
    <InputMask mask={ "9999 9999 9999 9999" }
               onChange={ onChange }
               type={ "tel" }
               value={ value }>
      <input { ...otherProps }/>
    </InputMask>
  )
}


const CodeInput = (props: InputInstanceProps) => {
  const {
    setValue,
    value,

    ...otherProps
  } = props

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value)
  }

  return (
    <InputMask mask={ '999-999' }
               value={ value }
               onChange={ onChange }>
      <input type={ "tel" }
             { ...otherProps }/>
    </InputMask>
  )
}