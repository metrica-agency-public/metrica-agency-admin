import React from "react";
import cn from 'classnames'

import styles from "./ToggleOptions.module.scss";

export enum ToggleStateType {
  LEFT,
  RIGHT,
}

interface ToggleOptionsProps {
  switchState: ToggleStateType
  options: (string | number)[]
}

export const ToggleOptions = (props: React.HTMLProps<HTMLDivElement> & ToggleOptionsProps) => {
  const { switchState, options, className, ...otherProps } = props;

  return (
    <div className={cn(styles.container, className)}
         { ...otherProps }>
      { options.map((option, index) => (
        <p key={ option }
           className={ styles.option }
           data-option=""
           data-switch-state={ switchState === index }>{ option }</p>
      )) }
    </div>
  );
};