import React, { HTMLAttributes } from "react";
import cn from 'classnames'

import styles from "./ButtonBack.module.scss";
import { ReactComponent as ArrowRight } from "assets/svg/arrow-left.svg";

interface ButtonBackProps {
  text?: string
}

export const ButtonBack = (props: React.PropsWithoutRef<HTMLAttributes<HTMLButtonElement>> & ButtonBackProps) => {
  const { text, className, ...otherProps } = props;

  return (
    <button className={cn(styles.container, className)}
            { ...otherProps }>
      <ArrowRight className={ styles.arrow }/>
      <p className={ styles.text }>{ text }</p>
    </button>
  );
}