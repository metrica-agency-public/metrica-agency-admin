import React from 'react';
import cn from 'classnames'

import styles from "./ValidateMessage.module.scss";


interface ValidateMessageProps {
  msg: string
  isActive: boolean
}

export const ValidateMessage = (props: ValidateMessageProps & {className?: string}) => {
  const { msg, isActive, className } = props;
  return (
    <p className={cn(styles.message, className)} data-active={ isActive }>{ msg }</p>
  );
};