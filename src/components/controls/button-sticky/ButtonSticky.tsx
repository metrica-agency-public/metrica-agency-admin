import React, { ButtonHTMLAttributes } from "react";
import styles from "./ButtonSticky.module.scss";
import { ReactComponent as Arrow } from "assets/svg/arrow-right.svg";
import cn from "classnames";

interface ButtonStickyProps {
  text: string
}

export const ButtonSticky = React.forwardRef((
  props: React.PropsWithRef<ButtonHTMLAttributes<HTMLButtonElement>> & ButtonStickyProps,
  ref: React.Ref<HTMLButtonElement>) => {
  const { className, text, ...otherProps } = props;

  return (
    <button className={ cn(styles.container, className) }
            ref={ ref }
            { ...otherProps }>
      <p className={ styles.text }>{ text }</p>
      <Arrow className={ styles.arrow }/>
    </button>
  );
});
