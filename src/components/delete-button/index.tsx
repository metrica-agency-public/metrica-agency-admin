import { DeleteOutlined } from "@ant-design/icons";
import { Button } from "antd";

type Props = {
  onClick: (e: any) => void;
  text?: string;
  showIcon?: boolean;
};

export const DeleteButton = (props: Props) => {
  return (
    <Button
      type="primary"
      icon={<DeleteOutlined />}
      onClick={props.onClick}
      style={{
        background: "#FF6347",
        border: "none",
      }}
    >
      {props.text}
    </Button>
  );
};
