import { useEffect, useState } from "react";
import { ChromePicker, ColorResult } from "react-color";

interface Props {
  value?: string;
  onChange?: (color: string) => void;
}

export const ColorPicker = (props: Props) => {
  const [color, setColor] = useState<string>("");

  useEffect(() => {
    if (props.value) {
      setColor(props.value);
    }
  }, [props.value]);

  const handleColor = (color: ColorResult) => {
    setColor(color.hex);
    if (props.onChange) {
      props.onChange(color.hex);
    }
  };

  return <ChromePicker color={color} onChange={handleColor} />;
};
