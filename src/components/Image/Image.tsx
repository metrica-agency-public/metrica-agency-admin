import React from "react";
import styles from "./Image.module.scss";
import cn from 'classnames'

export const Image = (props: React.PropsWithChildren<any>) => {
  const { children, className, ...otherProps } = props;

  return (
    <div className={cn(styles.image, className)} { ...otherProps }>
      { children }
    </div>
  );
}