import { Layout, Menu } from 'antd';
import { ROUTES } from 'router';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useHistory } from 'react-router-dom';
import styles from './styles.module.scss';

export const NavBlock = observer(() => {
  const history = useHistory();

  const toPage = (url: string) => {
    history.push(url);
  };

  return (
    <Layout.Sider className={styles.navBlock}>
      <Menu theme="light" mode="inline" className={styles.menu}>
        <Menu.Item key="1" onClick={() => toPage(ROUTES.ADMIN.STACK.path)}>
          Стэк
        </Menu.Item>
        <Menu.Item key="2" onClick={() => toPage(ROUTES.ADMIN.PROJECTS.path)}>
          Проекты
        </Menu.Item>
        <Menu.Item key="3" onClick={() => toPage(ROUTES.ADMIN.MEMBERS.path)}>
          Команда
        </Menu.Item>
        <Menu.Item key="4" onClick={() => toPage(ROUTES.ADMIN.SERVICES.path)}>
          Услуги
        </Menu.Item>
        <Menu.Item key="5" onClick={() => toPage(ROUTES.ADMIN.META_INFO.path)}>
          Мета-Инфо
        </Menu.Item>
      </Menu>
    </Layout.Sider>
  );
});
