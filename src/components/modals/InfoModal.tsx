import { Modal } from 'antd';

export const showInfo = (content: string) => {
  Modal.info({
    title: 'Уведомление',
    content,
    onOk() {},
  });
};
