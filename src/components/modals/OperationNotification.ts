import { notification } from 'antd';

export const openNotification = (description: string) => {
  notification.info({
    message: description,
    description: '',
    placement: 'bottomRight',
  });
};
