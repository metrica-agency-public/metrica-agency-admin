import { Modal } from 'antd';

export const showError = (content: string) => {
  Modal.error({
    title: 'This is an error message',
    content,
  });
};
