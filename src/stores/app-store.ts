import { API } from "api";
import { AuthAPI } from "api/auth/api";
import { AxiosRequestConfig } from "axios";
import { openNotification } from "components/modals/OperationNotification";
import { makeAutoObservable } from "mobx";
import { Constants } from "utils/constants";
import UtilsENVConfig from "utils/utils-env-config";

class AppStore {
  isInit: boolean = false;
  ACCESS_TOKEN_TITLE: string = "ACCESS_TOKEN";
  X_ACCESS_TOKEN_TITLE: string = "x-access-token";

  constructor() {
    makeAutoObservable(this);
  }

  async initApp() {
    console.log("URL =", UtilsENVConfig.getProcessEnv().REACT_APP_BACK_URL);
    API.setHost(Constants.API_V1_URL);

    API.addRequestInterceptor(
      (config: AxiosRequestConfig<any>) => {
        const accessToken = window.localStorage.getItem(this.ACCESS_TOKEN_TITLE) ?? "";
        return {
          ...config,
          headers: {
            ...config.headers,
            [this.X_ACCESS_TOKEN_TITLE]: accessToken,
          },
        };
      },
      (error: any) => error
    );

    const token = window.localStorage.getItem(this.ACCESS_TOKEN_TITLE) ?? "";

    //Токен есть в локале
    if (token.length !== 0) {
      const resp = await AuthAPI.checkToken();
      if (resp.isSuccess) {
        this.isInit = true;
      } else {
        window.localStorage.removeItem(this.ACCESS_TOKEN_TITLE);
        window.location.reload();
      }
    } else {
      //Токена нет
      this.isInit = true;
    }
  }

  login = async (login: string, password: string) => {
    const resp = await AuthAPI.login({
      login,
      password,
    });
    if (resp.isSuccess) {
      if (resp.data) {
        console.log("LOGIN =", resp.data);
        this.setAuth(resp.data.accessToken);
        window.location.reload();
      }
    } else {
      openNotification(resp.message ?? "Ошибка авторизации");
    }
  };

  logout = async () => {
    const resp = await AuthAPI.logout();
    window.localStorage.removeItem(this.ACCESS_TOKEN_TITLE);
    window.location.reload();
  };

  setAuth = (accessToken: string) => {
    window.localStorage.setItem(this.ACCESS_TOKEN_TITLE, accessToken);
  };

  token() {
    return window.localStorage.getItem(this.ACCESS_TOKEN_TITLE) ?? "";
  }

  isAuth() {
    const token = window.localStorage.getItem(this.ACCESS_TOKEN_TITLE) ?? "";
    return token.length !== 0;
  }
}

export default new AppStore();
