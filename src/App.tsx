import { BaseContainer } from "components/base-container";
import { ROUTES } from "router";
import { observer } from "mobx-react-lite";
import { Pages } from "pages";
import { LoginScreen } from "pages/auth";
import LoginVM from "pages/auth/vm";
import { Loader } from "pages/loader";
import React, { useEffect } from "react";
import { Redirect, Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import appStore from "stores/app-store";
import "./App.scss";

const App = observer(() => {
  useEffect(() => {
    appStore.initApp();
  }, []);

  return (
    <>
      {appStore.isInit ? (
        appStore.isAuth() ? (
          <BaseContainer>
            <Pages />
          </BaseContainer>
        ) : (
          <>
            <Route exact path={ROUTES.ADMIN.LOGIN.path}>
              <LoginScreen vm={new LoginVM()} />
            </Route>

            <Redirect to={ROUTES.ADMIN.LOGIN.path} />
          </>
        )
      ) : (
        <Loader />
      )}

      {/* <UploadImage /> */}

      {/* escrow: {
                startAccumulation: true, // - Используется для первой оплаты - для создания накопления
                AccumulationId: "4733183a-c6f6-4c1b-8ff1-fb7c43ed1695", // - Оплата по уже созданному накоплению.
            },  */}

      {/* <PaymentButton
        description="Test accumulation"
        accountId="4444"
        amount={11}
        invoiceId="0000"
        // escrow={{
          // escrowType: 1,
          // startAccumulation: true,
        // }}
      /> */}

      {/* <TGOauth /> */}

      <ToastContainer position="bottom-center" autoClose={3000} hideProgressBar={false} />
    </>
  );
});

export default App;
