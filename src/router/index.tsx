export const ROUTES = {
  ROOT: {
    path: "/",
  },
  ADMIN: {
    path: "/admin",
    LOGIN: {
      path: "/login",
    },
    STACK: {
      path: "/stack",
    },
    PROJECTS: {
      path: "/projects",
    },
    MEMBERS: {
      path: "/members",
    },
    SERVICES: {
      path: "/services",
    },
    META_INFO: {
      path: "/meta-info",
    },
  },
};

export const createURL = (path: string) => `https://${window.location.host}${path}`;
