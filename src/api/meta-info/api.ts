import { API, ResMessageOk } from "api";
import { ReqMetaInfoCreate, ReqMetaInfoSearch, ResMetaInfo, ResMetaInfoList } from "./classes";

const PATHS = {
  META_INFO: "/sa/meta-info",
  META_INFO_ID: (id: string) => `${PATHS.META_INFO}/${id}`,
};

export const MetaInfoAPI = {
  getMetaInfoList: async (params?: ReqMetaInfoSearch) => {
    return await API.get<ResMetaInfoList>({
      url: PATHS.META_INFO,
      params,
    });
  },

  addMetaInfo: async (data: ReqMetaInfoCreate) => {
    return await API.post<ResMetaInfo>({
      url: PATHS.META_INFO,
      data,
    });
  },

  patchMetaInfo: async (id: string, data: ReqMetaInfoCreate) => {
    return await API.patch<ResMetaInfo>({
      url: PATHS.META_INFO_ID(id),
      data,
    });
  },

  deleteMetaInfo: async (id: string) => {
    return await API.delete<ResMessageOk>({
      url: PATHS.META_INFO_ID(id),
    });
  },
};
