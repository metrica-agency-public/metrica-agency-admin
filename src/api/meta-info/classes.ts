
export interface ResMetaInfo {
  id: string;
  title_ru: string;
  description_ru: string;
  title_en: string;
  description_en: string;
  serialNumber: number;
}

export interface ResMetaInfoList {
  metaInfoList: ResMetaInfo[];
}

export interface ReqMetaInfoCreate {
  title_ru: string;
  description_ru: string;
  title_en: string;
  description_en: string;
  serialNumber: number;
}

export interface ReqMetaInfoSearch {
  searchValue: string;
}

