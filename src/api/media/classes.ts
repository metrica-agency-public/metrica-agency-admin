export interface ResUploadFileList {
  fileList: ResUploadFile[];
}

export interface ResUploadFile {
  id: string;
  extension: string;
  size: number;
  originalName: string;
  type: string;
  url: string;
}
