import { API } from 'api';
import { ResUploadFile, ResUploadFileList } from './classes';

const MEDIA_PATHS = {
  MEDIA: '/sa/media',
};

export const MediaAPI = {
  getFiles: async () => {
    return await API.get<ResUploadFileList>({ url: MEDIA_PATHS.MEDIA });
  },

  uploadFile: async (file: any) => {
    const formData = new FormData();
    formData.append('file', file);
    return await API.post<ResUploadFile>({ url: MEDIA_PATHS.MEDIA, data: formData });
  },
};
