import { API, ResMessageOk } from "api";
import {
  ReqMemberContactCreate,
  ReqMemberCreate,
  ReqMemberSearch,
  ReqMemberStackCreate,
  ReqMemberStackUpdate,
  ResMember,
  ResMemberList,
} from "./classes";

const PATHS = {
  MEMBERS: "/sa/members",
  MEMBERS_ID: (id: string) => `${PATHS.MEMBERS}/${id}`,
  MEMBERS_ID_RESTORE: (id: string) => `${PATHS.MEMBERS_ID(id)}/restore`,
  MEMBERS_ID_FOREVER: (id: string) => `${PATHS.MEMBERS_ID(id)}/forever`,

  MEMBERS_ID_CONTACTS: (id: string) => `${PATHS.MEMBERS_ID(id)}/contacts`,
  MEMBERS_ID_CONTACTS_ID: (memberId: string, contactId: string) =>
    `${PATHS.MEMBERS_ID_CONTACTS(memberId)}/${contactId}`,

  MEMBERS_ID_STACK: (id: string) => `${PATHS.MEMBERS_ID(id)}/stack`,
  MEMBERS_ID_STACK_ID: (memberId: string, stackId: string) => `${PATHS.MEMBERS_ID_STACK(memberId)}/${stackId}`,
};

export const MembersAPI = {
  getMemberList: async (params?: ReqMemberSearch) => {
    return await API.get<ResMemberList>({
      url: PATHS.MEMBERS,
      params,
    });
  },

  createMember: async (data: ReqMemberCreate) => {
    return await API.post<ResMember>({
      url: PATHS.MEMBERS,
      data,
    });
  },

  updateMember: async (id: string, data: ReqMemberCreate) => {
    return await API.patch<ResMember>({
      url: PATHS.MEMBERS_ID(id),
      data,
    });
  },

  deleteMember: async (id: string) => {
    return await API.delete<ResMember>({
      url: PATHS.MEMBERS_ID(id),
    });
  },

  deleteMemberForever: async (id: string) => {
    return await API.delete<ResMessageOk>({
      url: PATHS.MEMBERS_ID_FOREVER(id),
    });
  },

  restoreMember: async (id: string) => {
    return await API.patch<ResMember>({
      url: PATHS.MEMBERS_ID_RESTORE(id),
    });
  },

  //Работа с контактами
  addContact: async (memberId: string, data: ReqMemberContactCreate) => {
    return await API.post<ResMember>({
      url: PATHS.MEMBERS_ID_CONTACTS(memberId),
      data,
    });
  },

  updateContact: async (memberId: string, contactId: string, data: ReqMemberContactCreate) => {
    return await API.patch<ResMember>({
      url: PATHS.MEMBERS_ID_CONTACTS_ID(memberId, contactId),
      data,
    });
  },

  deleteContact: async (memberId: string, contactId: string) => {
    return await API.delete<ResMember>({
      url: PATHS.MEMBERS_ID_CONTACTS_ID(memberId, contactId),
    });
  },

  //Работа со стеком
  addStack: async (memberId: string, data: ReqMemberStackCreate) => {
    return await API.post<ResMember>({
      url: PATHS.MEMBERS_ID_STACK(memberId),
      data,
    });
  },

  updateStack: async (memberId: string, stackId: string, data: ReqMemberStackUpdate) => {
    return await API.patch<ResMember>({
      url: PATHS.MEMBERS_ID_STACK_ID(memberId, stackId),
      data,
    });
  },

  deleteStack: async (memberId: string, stackId: string) => {
    return await API.delete<ResMember>({
      url: PATHS.MEMBERS_ID_STACK_ID(memberId, stackId),
    });
  },
};
