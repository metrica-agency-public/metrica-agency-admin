import { ResUploadFile } from 'api/media/classes';
import { ResStack } from 'api/stack/classes';
import { FilterExistShowType } from 'utils/constants';

//-----RESPONSE-----

export interface ResMember {
  id: string;
  firstName_ru: string;
  lastName_ru: string;
  about_ru: string;
  skills_ru: string;

  firstName_en: string;
  lastName_en: string;
  about_en: string;
  skills_en: string; // DevOps,TeamLead, ...
  group: string;

  serialNumber: number;
  isExist: boolean;
  stackList: ResStack[];
  contactList: ResMemberContact[];

  fullImage: ResUploadFile;
  fullImageId: string;
  previewImage: ResUploadFile;
  previewImageId: string;
}

export interface ResMemberList {
  memberList: ResMember[];
}

export interface ResMemberContact {
  id: string;
  type: string;
  value: string;
  serialNumber: number;
}

//-----REQUEST-----

export interface ReqMemberCreate {
  firstName_ru: string;
  lastName_ru: string;
  about_ru: string;
  skills_ru: string; // DevOps,TeamLead, ...

  firstName_en: string;
  lastName_en: string;
  about_en: string;
  skills_en: string; // DevOps,TeamLead, ...

  serialNumber: number; // порядковый номер в списке
  previewImageId: string;
  fullImageId: string;
}

export interface ReqMemberContactCreate {
  type: string;
  value: string;
  serialNumber: number;
}

export interface ReqMemberStackCreate {
  stackId: string;
  serialNumber: number;
}

export interface ReqMemberStackUpdate {
  serialNumber: number;
}

export interface ReqMemberSearch {
  searchValue: string;
  show: FilterExistShowType;
}
