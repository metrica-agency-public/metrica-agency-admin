
//-----RESPONSE-----

export interface ResToken {
  accessToken: string;
}

//-----REQUEST-----

export interface ReqLogin {
  login: string;
  password: string;
}
