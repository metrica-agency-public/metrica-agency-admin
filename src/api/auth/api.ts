import { API, ResMessageOk } from "api";
import { ReqLogin, ResToken } from "./classes";

const PATHS = {
  AUTH: "/sa/auth",
  AUTH_LOGIN: () => `${PATHS.AUTH}/login`,
  AUTH_CHECK: () => `${PATHS.AUTH}/check`,
  AUTH_LOGOUT: () => `${PATHS.AUTH}/logout`,
};

export const AuthAPI = {
  login: async (data: ReqLogin) => {
    return await API.post<ResToken>({
      url: PATHS.AUTH_LOGIN(),
      data,
    });
  },

  checkToken: async () => {
    return await API.get<ResMessageOk>({
      url: PATHS.AUTH_CHECK(),
    });
  },

  logout: async () => {
    return await API.post({
      url: PATHS.AUTH_LOGOUT(),
    });
  },
};
