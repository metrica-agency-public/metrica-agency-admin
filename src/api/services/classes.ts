import { ResUploadFile } from 'api/media/classes';
import { FilterExistShowType } from 'utils/constants';

//-----RESPONSE-----

export interface ResService {
  id: string;
  title_ru: string;
  about_ru: string;
  title_en: string;
  about_en: string;
  serialNumber: number;
  isExist: boolean;
  image: ResUploadFile;
}

export interface ResServiceList {
  serviceList: ResService[];
}

//-----REQUEST-----

export interface ReqServiceCreate {
  title_ru: string;
  imageId: string;
  about_ru: string;
  title_en: string;
  about_en: string;
  serialNumber: number;
}
export interface ReqServiceSearch {
  searchValue: string;
  show: FilterExistShowType;
}

