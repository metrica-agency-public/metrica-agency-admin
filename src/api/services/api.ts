import { API, ResMessageOk } from "api";
import { ReqServiceCreate, ReqServiceSearch, ResService, ResServiceList } from "./classes";

const PATHS = {
  SERVICE: "/sa/services",
  SERVICE_ID: (id: string) => `${PATHS.SERVICE}/${id}`,
  SERVICE_ID_RESTORE: (id: string) => `${PATHS.SERVICE_ID(id)}/restore`,
  SERVICE_ID_FOREVER: (id: string) => `${PATHS.SERVICE_ID(id)}/forever`,
};

export const ServiceAPI = {
  getServices: async (params?: ReqServiceSearch) => {
    return await API.get<ResServiceList>({
      url: PATHS.SERVICE,
      params,
    });
  },

  createService: async (data: ReqServiceCreate) => {
    return await API.post<ResService>({
      url: PATHS.SERVICE,
      data,
    });
  },

  patchService: async (id: string, data: ReqServiceCreate) => {
    return await API.patch<ResService>({
      url: PATHS.SERVICE_ID(id),
      data,
    });
  },

  deleteService: async (id: string) => {
    return await API.delete<ResService>({
      url: PATHS.SERVICE_ID_FOREVER(id),
    });
  },

  deleteServiceForever: async (id: string) => {
    return await API.delete<ResMessageOk>({
      url: PATHS.SERVICE_ID_FOREVER(id),
    });
  },

  restoreService: async (id: string) => {
    return await API.patch<ResService>({
      url: PATHS.SERVICE_ID_RESTORE(id),
    });
  },
};
