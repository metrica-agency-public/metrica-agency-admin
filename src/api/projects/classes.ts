import { ResUploadFile } from 'api/media/classes';
import { ResStack } from 'api/stack/classes';
import { FilterExistShowType } from 'utils/constants';

//-----RESPONSE-----

export interface ResProjectProperty {
  id: string;
  title_ru: string;
  description_ru: string;
  title_en: string;
  description_en: string;
  serialNumber: number;
  imagePosition: string;
  image: ResUploadFile;
}

export interface ResProject {
  id: string;
  logo: ResUploadFile;
  image: ResUploadFile;
  video: ResUploadFile;
  title_ru: string;
  about_ru: string;
  title_en: string;
  about_en: string;
  color: string;
  serialNumber: number;
  isExist: boolean;
  propertyList: ResProjectProperty[];
  stackList: ResStack[];
}

export interface ResProjectList {
  projectList: ResProject[];
}

//-----REQUEST-----

export interface ReqProjectCreate {
  title_ru: string;
  about_ru: string;
  title_en: string;
  about_en: string;
  logoId: string;
  imageId: string;
  videoId: string | null;
  color: string; // ЦВЕТ - в формате HEX
  serialNumber: number;
}

export interface ReqProjectPropertyCreate {
  title_ru: string;
  description_ru: string;
  title_en: string;
  description_en: string;
  serialNumber: number;
  imageId: string;
  imagePosition: string; // Тип == позиция ! (Относительно текста)
}

export interface ReqProjectStackCreate {
  stackId: string;
  serialNumber: number;
}

export interface ReqProjectStackUpdate {
  serialNumber: number;
}

export interface ReqProjectsSearch {
  searchValue: string;
  show: FilterExistShowType;
}