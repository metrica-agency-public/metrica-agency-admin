import { API, ResMessageOk } from "api";
import {
  ReqProjectCreate,
  ReqProjectPropertyCreate,
  ReqProjectsSearch,
  ReqProjectStackCreate,
  ReqProjectStackUpdate,
  ResProject,
  ResProjectList,
} from "./classes";

const PATHS = {
  PROJECTS: "/sa/projects",
  PROJECTS_ID: (id: string) => `${PATHS.PROJECTS}/${id}`,
  PROJECTS_ID_RESTORE: (id: string) => `${PATHS.PROJECTS_ID(id)}/restore`,
  PROJECTS_ID_FOREVER: (id: string) => `${PATHS.PROJECTS_ID(id)}/forever`,

  PROJECTS_ID_PROPERTY: (id: string) => `${PATHS.PROJECTS_ID(id)}/project-property`,
  PROJECTS_ID_PROPERTY_ID: (projectId: string, propertyId: string) =>
    `${PATHS.PROJECTS_ID_PROPERTY(projectId)}/${propertyId}`,

  PROJECTS_ID_STACK: (id: string) => `${PATHS.PROJECTS_ID(id)}/stack`,
  PROJECTS_ID_STACK_ID: (projectId: string, stackId: string) => `${PATHS.PROJECTS_ID_STACK(projectId)}/${stackId}`,
};

export const ProjectsAPI = {
  getProjectList: async (params?: ReqProjectsSearch) => {
    return await API.get<ResProjectList>({
      url: PATHS.PROJECTS,
      params,
    });
  },

  createProject: async (req: ReqProjectCreate) => {
    return await API.post<ResProject>({
      url: PATHS.PROJECTS,
      data: req,
    });
  },

  updateProject: async (id: string, req: ReqProjectCreate) => {
    return await API.patch<ResProject>({
      url: PATHS.PROJECTS_ID(id),
      data: req,
    });
  },

  deleteProject: async (id: string) => {
    return await API.delete<ResProject>({
      url: PATHS.PROJECTS_ID(id),
    });
  },

  deleteProjectForever: async (id: string) => {
    return await API.delete<ResMessageOk>({
      url: PATHS.PROJECTS_ID_FOREVER(id),
    });
  },

  restoreProject: async (id: string) => {
    return await API.patch<ResProject>({
      url: PATHS.PROJECTS_ID_RESTORE(id),
    });
  },

  //Работа со свойствами проектов
  addProperty: async (projectId: string, data: ReqProjectPropertyCreate) => {
    return await API.post<ResProject>({
      url: PATHS.PROJECTS_ID_PROPERTY(projectId),
      data,
    });
  },

  updateProperty: async (projectId: string, propertyId: string, data: ReqProjectPropertyCreate) => {
    return await API.patch<ResProject>({
      url: PATHS.PROJECTS_ID_PROPERTY_ID(projectId, propertyId),
      data,
    });
  },

  deleteProperty: async (projectId: string, propertyId: string) => {
    return await API.delete<ResProject>({
      url: PATHS.PROJECTS_ID_PROPERTY_ID(projectId, propertyId),
    });
  },

  //Работа со стеком
  addStack: async (projectId: string, data: ReqProjectStackCreate) => {
    return await API.post<ResProject>({
      url: PATHS.PROJECTS_ID_STACK(projectId),
      data,
    });
  },

  updateStack: async (projectId: string, stackId: string, data: ReqProjectStackUpdate) => {
    return await API.patch<ResProject>({
      url: PATHS.PROJECTS_ID_STACK_ID(projectId, stackId),
      data,
    });
  },

  deleteStack: async (projectId: string, stackId: string) => {
    return await API.delete<ResProject>({
      url: PATHS.PROJECTS_ID_STACK_ID(projectId, stackId),
    });
  },
};
