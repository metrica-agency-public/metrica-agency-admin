import { NetClient } from "network/net-client";

export const API = new NetClient();

export interface ResMessageOk {
  message: string;
}