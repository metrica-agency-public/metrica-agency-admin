import { ResUploadFile } from 'api/media/classes';
import { FilterExistShowType } from 'utils/constants';

export interface ResStack {
  id: string;
  image: ResUploadFile;
  title_ru: string;
  description_ru: string;
  title_en: string;
  description_en: string;
  ProjectStack?: ProjectStack
  MemberStack?: MemberStack
}

export interface ReqStack {
  title_ru: string;
  description_ru: string;
  title_en: string;
  description_en: string;
  imageId: string;
}

export interface ResStackList {
  stackList: ResStack[];
}
export interface ProjectStack {
  serialNumber: number;
}

export interface MemberStack {
  serialNumber: number;
}

export interface ReqStackSearch {
  searchValue: string;
  show: FilterExistShowType;
}



