import { API, ResMessageOk } from "api";
import { ReqStack, ReqStackSearch, ResStack, ResStackList } from "./classes";

const PATHS = {
  STACK: "/sa/stack",
  STACK_ID: (id: string) => `${PATHS.STACK}/${id}`,
};

export const StackAPI = {
  getStackList: async (params?: ReqStackSearch) => {
    return await API.get<ResStackList>({
      url: PATHS.STACK,
      params,
    });
  },

  createStack: async (data: ReqStack) => {
    return await API.post<ReqStack>({
      url: PATHS.STACK,
      data,
    });
  },

  updateStack: async (id: string, data: ReqStack) => {
    return await API.patch<ResStack>({
      url: PATHS.STACK_ID(id),
      data,
    });
  },

  deleteStack: async (id: string) => {
    return await API.delete<ResMessageOk>({
      url: PATHS.STACK_ID(id),
    });
  },
};
