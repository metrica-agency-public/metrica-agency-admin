import { Socket } from "socket.io-client";

export enum SocketPaymentEventType {
  // TODO : confirm reg, notifications
  SUCCESS_PAYMENT = 'SUCCESS_PAYMENT',
  FAIL_PAYMENT = 'FAIL_PAYMENT',
}

export enum SocketEvents {
  REGISTRATION = 'REGISTRATION',
}

export enum DefaultSocketOnEvents {
  CONNECT = 'connect',
  DISCONNECT = 'disconnect',
  CONNECT_ERROR = 'connect_error',
}

export interface IOnDefaultSocketListener {
  event: DefaultSocketOnEvents;
  onConnect?: () => void;
  onDisconnect?: (reason: Socket.DisconnectReason) => void;
  onConnectError?: (err: Error) => void;
}