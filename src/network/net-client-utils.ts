import { AxiosResponse } from 'axios';
import { INetResult } from './net-client-types';

//Дефолтные заголовки для отправки файлов на сервер
export const MEDIA_HEADERS = {
  'Content-Type': 'multipart/form-data',
};

export function handleAxiosSuccess<T = any>(
  response: AxiosResponse<T, any>,
): INetResult<T> {
  console.log('NetClient: handleSuccess = ', {
    data: response.data,
    status: response.status,
    statusText: response.statusText,
    headers: response.headers,
    config: response.config,
  });

  return {
    isSuccess: true,
    code: response.status,
    data: response.data,
  };
}

//обработчик ошибок
export function handleAxiosError<T = any>(error: any): INetResult<T> {
  console.log('NetClient: handleError');

  printAxiosErrorLog(error);

  const result: INetResult<T> = {
    isSuccess: false,
    code: 500,
    message: 'Произошла неизвестная ошибка',
  };

  if (error.response) {
    //Достаем код
    if (typeof error.response.status === 'number') {
      result.code = error.response.status;
    }
    //Достаем сообщение + данные
    if (error.response.data) {
      //Если в ошибке есть сообщение
      if (typeof error.response.data.message === 'string') {
        result.message = error.response.data.message;
      }

      //Если в ошибке есть errorData
      if (error.response.data.errorData) {
        result.errorData = error.response.data.errorData;
      } else {
        result.errorData = error.response.data;
      }
    }
  }

  return result;
}

export function printAxiosErrorLog(error: any) {
  if (error.isAxiosError) {
    //Если там в ошибке нихрена нет -> сразу отдаем данные об этом
    if (error.response) {
      console.log('NetClient ERROR (AXIOS RESPONSE)', {
        data: error.response.data,
        status: error.response.status,
        statusText: error.response.statusText,
        headers: error.response.headers,
      });
    } else {
      console.log('NetClient ERROR (AXIOS, NO RESPONSE)', error);
    }
  } else {
    console.log('NetClient ERROR (NO AXIOS)', error);
  }
}

export function makeDate() {
  const formatter = new Intl.DateTimeFormat('ru', {
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
  });
  return formatter.format(new Date());
}
