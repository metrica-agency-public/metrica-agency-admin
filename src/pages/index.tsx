import { ROUTES } from 'router';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { Route } from 'react-router-dom';
import { MemberListScreen } from './member-list';
import MemberListVM from './member-list/vm';
import { MetaInfoListScreen } from './meta-Info-list';
import MetaInfoVM from './meta-Info-list/vm';
import { ProjectListScreen } from './project-list';
import ProjectListVM from './project-list/vm';
import { ServiceListScreen } from './service-list';
import ServiceListVM from './service-list/vm';
import { StackListScreen } from './stack-list';
import StackListVM from './stack-list/vm';

export const Pages = observer(() => {
  return (
    <>
      <Route exact path={ROUTES.ADMIN.STACK.path}>
        <StackListScreen vm={new StackListVM()} />
      </Route>

      <Route exact path={ROUTES.ADMIN.MEMBERS.path}>
        <MemberListScreen vm={new MemberListVM()} />
      </Route>

      <Route exact path={ROUTES.ADMIN.SERVICES.path}>
        <ServiceListScreen vm={new ServiceListVM()} />
      </Route>

      <Route exact path={ROUTES.ADMIN.PROJECTS.path}>
        <ProjectListScreen vm={new ProjectListVM()} />
      </Route>

      <Route exact path={ROUTES.ADMIN.META_INFO.path}>
        <MetaInfoListScreen vm={new MetaInfoVM()} />
      </Route>
    </>
  );
});
