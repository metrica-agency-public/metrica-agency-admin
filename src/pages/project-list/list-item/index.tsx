import { Button, Card as BaseCard, Collapse, Image, Space } from 'antd';
import { ResProject, ResProjectProperty } from 'api/projects/classes';
import { ResStack } from 'api/stack/classes';
import cn from 'classnames';
import { DeleteButton } from 'components/delete-button';
import { EditButton } from 'components/edit-button';
import { confirmDialog } from 'components/modals/SureModal';
import { observer } from 'mobx-react-lite';
import ProjectListVM from '../vm';
import styles from './styles.module.scss';

interface Props {
  vm: ProjectListVM;
  item: ResProject;
  className?: string;
}

const View = ({ vm, className, item }: Props) => {
  const onEdit = () => {
    vm.openUpdateModal(item);
  };

  const onDelete = () => {
    confirmDialog({
      onOk: () => {
        vm.deleteProject(item.id);
      },
    });
  };

  const onDeleteForever = () => {
    confirmDialog({
      onOk: () => {
        vm.deleteProjectForever(item.id);
      },
    });
  };

  const onRestore = () => {
    confirmDialog({
      onOk: () => {
        vm.restoreProject(item.id);
      },
    });
  };

  //Работа со стеком
  const onEditStack = (stack: ResStack) => {
    vm.openUpdateStackModal(item, stack);
  };

  const onDeleteStack = (stackId: string) => {
    confirmDialog({
      onOk: () => {
        vm.deleteStack(item.id, stackId);
      },
    });
  };

  //Работа с контактами
  const onEditProperty = (resProperty: ResProjectProperty) => {
    vm.openUpdatePropertyModal(item, resProperty);
  };

  const onDeleteProperty = (propertyId: string) => {
    confirmDialog({
      onOk: () => {
        vm.deleteProperty(item.id, propertyId);
      },
    });
  };

  return (
    <>
      <BaseCard
        className={cn(styles.root, className)}
        title={item.title_ru}
        extra={
          <>
            {item.isExist && (
              <Space size="small" direction="horizontal">
                <EditButton onClick={onEdit} />
                <DeleteButton onClick={onDelete} />
              </Space>
            )}
            {!item.isExist && (
              <Space size="small" direction="horizontal">
                <Button type="primary" onClick={onRestore}>
                  Восстановить
                </Button>
                <DeleteButton text="Удалить навсегда" onClick={onDeleteForever} />
              </Space>
            )}
          </>
        }
      >
        <div className={styles.field}>{'Тайтл(EN): ' + item.title_en}</div>
        <div className={styles.field}>{'Описание(RU): ' + item.about_ru}</div>
        <div className={styles.field}>{'Описание(EN): ' + item.about_en}</div>
        <div className={styles.field}>{'Цвет: ' + item.color}</div>
        <div className={styles.field}>{'Порядковый номер: ' + item.serialNumber}</div>
        <div className={styles.field}>{'isExist: ' + item.isExist}</div>
        <Collapse>
          <Collapse.Panel header="Стек" key="1">
            <div className={styles.tags}>
              <div className={styles.row}>
                <div>
                  Стек:
                  <div>
                    <Button className={styles.button} type="primary" onClick={() => vm.openAddStackModal(item)}>
                      Добавить стек
                    </Button>
                  </div>
                </div>
              </div>
              {item.stackList?.map((stack) => {
                return (
                  <BaseCard
                    key={stack.id}
                    title={stack.title_ru}
                    extra={
                      <Space size="small" direction="horizontal">
                        <EditButton onClick={() => onEditStack(stack)} />
                        <DeleteButton onClick={() => onDeleteStack(stack.id)} />
                      </Space>
                    }
                  >
                    <div className={styles.field}>{'Описание(RU): ' + stack.description_ru}</div>
                    <div className={styles.field}>{'Описание(EN): ' + stack.description_en}</div>
                    <div className={styles.field}>{'Позиция: ' + stack.ProjectStack?.serialNumber}</div>
                    <Image width={200} src={stack.image.url} />
                  </BaseCard>
                );
              })}
            </div>
          </Collapse.Panel>
          <Collapse.Panel header="Свойства проекта" key="2">
            <div className={styles.tags}>
              <div className={styles.row}>
                <div>
                  Свойства проекта:
                  <div>
                    <Button className={styles.button} type="primary" onClick={() => vm.openAddPropertyModal(item)}>
                      Добавить свойство
                    </Button>
                  </div>
                </div>
              </div>
              {item.propertyList?.map((property) => {
                return (
                  <BaseCard
                    key={property.id}
                    extra={
                      <Space size="small" direction="horizontal">
                        <EditButton onClick={() => onEditProperty(property)} />
                        <DeleteButton onClick={() => onDeleteProperty(property.id)} />
                      </Space>
                    }
                  >
                    <div className={styles.field}>{'Title(RU): ' + property.title_ru}</div>
                    <div className={styles.field}>{'Description(RU): ' + property.description_ru}</div>
                    <div className={styles.field}>{'Title(EN): ' + property.title_en}</div>
                    <div className={styles.field}>{'Description(EN): ' + property.description_en}</div>
                    <div className={styles.field}>
                      {'Расположение картинки: ' + vm.getPropertyImagePositionRUS(property.imagePosition)}
                    </div>
                    <div className={styles.field}>{'Порядковый номер: ' + property.serialNumber}</div>
                    <div className={styles.field}>{'Картинка: '}</div>
                    <Image width={200} src={property.image?.url} />
                  </BaseCard>
                );
              })}
            </div>
          </Collapse.Panel>
        </Collapse>
        <div className={styles.row}>
          <div>
            <div className={styles.field}>{'Логотип: '}</div>
            <Image width={200} src={item.logo?.url} />
          </div>
          <div>
            <div className={styles.field}>{'Картинка фулл сайз: '}</div>
            <Image width={200} src={item.image?.url} />
          </div>
          <div>
            <div className={styles.field}>{'Видео: '}</div>
            <Image width={200} src={item.video?.url} />
          </div>
        </div>
      </BaseCard>
    </>
  );
};

export const ProjectListItem = observer(View);
