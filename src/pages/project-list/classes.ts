import { ResUploadFile } from "api/media/classes";

export interface ProjectCreateFormValues {
  title_ru: string;
  title_en: string;
  
  about_ru: string;
  about_en: string;

  serialNumber: number;

  color: string;

  logo: ResUploadFile;
  image: ResUploadFile;
  video?: ResUploadFile;
}

//Работа со стеком
export interface ProjectStackCreateFormValues {
  stackId: string;
  serialNumber: number;
}

export interface ProjectStackUpdateFormValues {
  serialNumber: number;
}

//Работа со свойствами
export interface ProjectPropertyCreateFormValues {
  title_ru: string;
  title_en: string;

  description_ru: string;
  description_en: string;
  
  imagePosition: string;
  serialNumber: number;

  image: ResUploadFile;
}