import { ProjectsAPI } from "api/projects/api";
import { ResProject, ResProjectProperty } from "api/projects/classes";
import { StackAPI } from "api/stack/api";
import { ResStack } from "api/stack/classes";
import { openNotification } from "components/modals/OperationNotification";
import { makeAutoObservable } from "mobx";
import {
  FilterExistShowType,
  ProjectPropertyImagePositionRUS
} from "utils/constants";
import {
  ProjectCreateFormValues,
  ProjectPropertyCreateFormValues,
  ProjectStackCreateFormValues,
  ProjectStackUpdateFormValues
} from "./classes";

class ProjectListVM {
  existShowType: FilterExistShowType = FilterExistShowType.ALL;
  searchValue: string = "";

  resProjectList: ResProject[] = [];

  //Создание и обновление проекта
  isCreateModal: boolean = false;
  projectItemForUpdate: ResProject | null = null;
  isUpdateModal: boolean = false;

  //Работа со стеком
  resStackList: ResStack[] = []; //Список стека который пришел с сервера
  isStackAddModal: boolean = false;
  isStackUpdateModal: boolean = false;
  stackItemForUpdate: ResStack | null = null; //Элемент для обновления стека
  stackIdAdd: string | null = null;

  //Работа с контактами
  isPropertyAddModal: boolean = false;
  isPropertyUpdateModal: boolean = false;
  propertyItemForUpdate: ResProjectProperty | null = null;

  constructor() {
    makeAutoObservable(this);
  }

  setSearchValue = (value: string) => {
    this.searchValue = value;
  };

  setFilterExistShowType = (existShowType: FilterExistShowType) => {
    this.existShowType = existShowType;
  };

  //Создание проекта
  openCreateModal() {
    this.isCreateModal = true;
  }

  closeCreateModal() {
    this.isCreateModal = false;
  }

  //Обновление проекта
  openUpdateModal(item: ResProject) {
    this.projectItemForUpdate = item;
    this.isUpdateModal = true;
  }

  closeUpdateModal() {
    this.isUpdateModal = false;
    this.projectItemForUpdate = null;
  }

  //Добавление стека
  openAddStackModal(projectItem: ResProject) {
    this.projectItemForUpdate = projectItem;
    this.stackIdAdd = null;
    this.isStackAddModal = true;
  }

  closeAddStackModal() {
    this.isStackAddModal = false;
    this.projectItemForUpdate = null;
    this.stackIdAdd = null;
  }

  //Обновление элемента стека
  openUpdateStackModal(projectItem: ResProject, stackItem: ResStack) {
    this.projectItemForUpdate = projectItem;
    this.stackItemForUpdate = stackItem;
    this.isStackUpdateModal = true;
  }

  closeUpdateStackModal() {
    this.isStackUpdateModal = false;
    this.projectItemForUpdate = null;
    this.stackItemForUpdate = null;
  }

  //Модалки для добавления и редактирования свойств проекта
  openAddPropertyModal(projectItem: ResProject) {
    this.projectItemForUpdate = projectItem;
    this.isPropertyAddModal = true;
  }

  closeAddPropertyModal() {
    this.isPropertyAddModal = false;
    this.projectItemForUpdate = null;
  }

  openUpdatePropertyModal(
    projectItem: ResProject,
    propertyItem: ResProjectProperty
  ) {
    this.projectItemForUpdate = projectItem;
    this.propertyItemForUpdate = propertyItem;
    this.isPropertyUpdateModal = true;
  }

  closeUpdatePropertyModal() {
    this.isPropertyUpdateModal = false;
    this.projectItemForUpdate = null;
    this.propertyItemForUpdate = null;
  }

  //Методы

  getPropertyImagePositionRUS(positionTypeEN: string) {
    let rusKeyValue = Object.entries(ProjectPropertyImagePositionRUS).find(
      (el) => el[0] === positionTypeEN
    );
    if (rusKeyValue) {
      return rusKeyValue[1];
    }
    return ProjectPropertyImagePositionRUS.RIGHT;
  }

  getStackList = async () => {
    const resp = await StackAPI.getStackList();
    if (resp.isSuccess) {
      if (resp.data) {
        this.resStackList = resp.data.stackList;
      }
    }
  };

  getProjectList = async () => {
    const resp = await ProjectsAPI.getProjectList({
      searchValue: this.searchValue ?? "",
      show: this.existShowType,
    });
    if (resp.isSuccess) {
      if (resp.data) {
        this.resProjectList = resp.data.projectList;
      }
    }
  };

  getFilterStackList = () => {
    let result: ResStack[] = [];
    if (this.projectItemForUpdate && this.resStackList) {
      this.resStackList.forEach((stack) => {
        const index = this.projectItemForUpdate!.stackList.findIndex(
          (el) => el.id === stack.id
        );
        if (index === -1) {
          result.push(stack);
        }
      });
    }
    return result;
  };

  createProject = async (formValues: ProjectCreateFormValues) => {
    const resp = await ProjectsAPI.createProject({
      ...formValues,
      logoId: formValues.logo.id,
      imageId: formValues.image.id,
      videoId: formValues.video ? formValues.video.id : null,
    });

    if (resp.isSuccess) {
      openNotification("Проект успешно создан");
      this.getProjectList();
    }
  };

  updateProject = async (formValues: ProjectCreateFormValues) => {
    if (!this.projectItemForUpdate) {
      return;
    }

    const resp = await ProjectsAPI.updateProject(this.projectItemForUpdate.id, {
      ...formValues,
      logoId: formValues.logo.id,
      imageId: formValues.image.id,
      videoId: formValues.video ? formValues.video.id : null,
    });

    if (resp.isSuccess) {
      openNotification("Проект успешно обновлен");
      if (resp.data) {
        this.updateProjectById(resp.data);
      }
    }
  };

  deleteProject = async (id: string) => {
    const resp = await ProjectsAPI.deleteProject(id);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateProjectById(resp.data);
      }
    }
  };

  restoreProject = async (id: string) => {
    const resp = await ProjectsAPI.restoreProject(id);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateProjectById(resp.data);
      }
    }
  };

  deleteProjectForever = async (id: string) => {
    const resp = await ProjectsAPI.deleteProjectForever(id);
    if (resp.isSuccess) {
      const index = this.resProjectList.findIndex((el) => el.id === id);
      if (index !== -1) {
        this.resProjectList.splice(index, 1);
      }
    }
  };

  updateProjectById(projectItem: ResProject) {
    const index = this.resProjectList.findIndex(
      (el) => el.id === projectItem.id
    );
    if (index !== -1) {
      this.resProjectList.splice(index, 1, projectItem);
    }
  }

  //Работа со стеком
  addStack = async (values: ProjectStackCreateFormValues) => {
    if (!this.projectItemForUpdate) return;

    const resp = await ProjectsAPI.addStack(
      this.projectItemForUpdate.id,
      values
    );
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateProjectById(resp.data);
      }
    }
  };

  updateStack = async (values: ProjectStackUpdateFormValues) => {
    if (!this.projectItemForUpdate || !this.stackItemForUpdate) return;

    const resp = await ProjectsAPI.updateStack(
      this.projectItemForUpdate.id,
      this.stackItemForUpdate.id,
      values
    );
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateProjectById(resp.data);
      }
    }
  };

  deleteStack = async (projectId: string, stackId: string) => {
    const resp = await ProjectsAPI.deleteStack(projectId, stackId);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateProjectById(resp.data);
      }
    }
  };

  //Работа со стеком
  addProperty = async (values: ProjectPropertyCreateFormValues) => {
    if (!this.projectItemForUpdate) return;

    const resp = await ProjectsAPI.addProperty(this.projectItemForUpdate.id, {
      ...values,
      imageId: values.image.id,
    });

    if (resp.isSuccess) {
      if (resp.data) {
        this.updateProjectById(resp.data);
      }
    }
  };

  updateProperty = async (values: ProjectPropertyCreateFormValues) => {
    if (!this.projectItemForUpdate || !this.propertyItemForUpdate) return;

    const resp = await ProjectsAPI.updateProperty(
      this.projectItemForUpdate.id,
      this.propertyItemForUpdate.id,
      {
        ...values,
        imageId: values.image.id,
      }
    );

    if (resp.isSuccess) {
      if (resp.data) {
        this.updateProjectById(resp.data);
      }
    }
  };

  deleteProperty = async (projectId: string, propertyId: string) => {
    const resp = await ProjectsAPI.deleteProperty(projectId, propertyId);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateProjectById(resp.data);
      }
    }
  };
}

export default ProjectListVM;
