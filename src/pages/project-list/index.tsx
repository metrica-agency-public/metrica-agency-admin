import { Button, Input, Select } from 'antd';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { FilterExistShowType } from 'utils/constants';
import { formatShowTypeRUS } from 'utils/helpers';
import { ProjectListItem } from './list-item';
import { ProjectCreateModal } from './modals/project-create';
import { ProjectUpdateModal } from './modals/project-update';
import { ProjectPropertyAddModal } from './modals/property-add';
import { ProjectPropertyUpdateModal } from './modals/property-update';
import { ProjectStackAddModal } from './modals/stack-add';
import { ProjectStackUpdateModal } from './modals/stack-update';
import styles from './styles.module.scss';
import ProjectListVM from './vm';

interface Props {
  vm: ProjectListVM;
}

const View = ({ vm }: Props) => {

  useEffect(() => {
    vm.getProjectList();
  }, [vm, vm.existShowType, vm.searchValue]);

  const onSearchChange = (e: any) => {
    vm.setSearchValue(e.target.value);
  };

  const onSearch = () => {
    vm.getProjectList();
  };

  const onExistShowChange = (value: FilterExistShowType) => {
    vm.setFilterExistShowType(value);
  };

  return (
    <div className={styles.root}>
      <h3 className={styles.title}>Проекты:</h3>
      <Input.Search
        value={vm.searchValue}
        onChange={onSearchChange}
        className={styles.search}
        placeholder="Поиск..."
        onSearch={onSearch}
        enterButton
      />
      <div className={styles.filterBlock}>
        <div className={styles.filterTitle}>Фильтрация</div>
        <div className={styles.row}>
          <div className={styles.filter}>
            По статусу:
            <Select value={vm.existShowType} style={{ width: 160 }} onChange={onExistShowChange}>
              <Select.Option value={FilterExistShowType.ALL}>{formatShowTypeRUS(FilterExistShowType.ALL)}</Select.Option>
              <Select.Option value={FilterExistShowType.EXISTING}>{formatShowTypeRUS(FilterExistShowType.EXISTING)}</Select.Option>
              <Select.Option value={FilterExistShowType.DELETED}>{formatShowTypeRUS(FilterExistShowType.DELETED)}</Select.Option>
            </Select>
          </div>
        </div>
      </div>
      <div className={styles.buttons}>
        <Button className={styles.button} type="primary" onClick={() => vm.openCreateModal()}>
          Создать проект
        </Button>
      </div>
      <div className={styles.tags}>
        {vm.resProjectList?.map((item) => {
          return <ProjectListItem key={item.id} vm={vm} item={item} className={styles.card} />;
        })}
      </div>
      <ProjectCreateModal vm={vm} />
      <ProjectUpdateModal vm={vm} />

      <ProjectStackAddModal vm={vm} />
      <ProjectStackUpdateModal vm={vm} />

      <ProjectPropertyAddModal vm={vm} />
      <ProjectPropertyUpdateModal vm={vm} />
    </div>
  );
};

export const ProjectListScreen = observer(View);
