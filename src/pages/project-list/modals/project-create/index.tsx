import { Button, Form, Input, InputNumber, Modal } from "antd";
import { useForm } from "antd/lib/form/Form";
import { ColorPicker } from "components/color-picker";
import { UploadImage } from "components/upload-image";
import { observer } from "mobx-react-lite";
import { ProjectCreateFormValues } from "pages/project-list/classes";
import ProjectListVM from "pages/project-list/vm";
import React from "react";

interface Props {
  vm: ProjectListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  const onFinish = async (values: ProjectCreateFormValues) => {
    await vm.createProject(values);
    form.resetFields();

    vm.closeCreateModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeCreateModal();
  }

  return (
    <Modal
      title="Добавление"
      visible={vm.isCreateModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form
        form={form}
        name="basic"
        key="0"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          key="1"
          label="Название(RU)"
          name="title_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          key="2"
          label="Название(EN)"
          name="title_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          key="3"
          label="Информация о проекте(RU)"
          name="about_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          key="4"
          label="Информация о проекте(EN)"
          name="about_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          key="5"
          label="Цвет"
          name="color"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <ColorPicker />
        </Form.Item>
        <Form.Item
          key="6"
          label="Порядоковый номер"
          name="serialNumber"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>

        <Form.Item
          key="7"
          label="Картинка на превью (Logo)"
          name="logo"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item
          key="8"
          label="Картинка на весь экран"
          name="image"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item
          key="9"
          label="Видео"
          name="video"
        >
          <UploadImage />
        </Form.Item>

        <Form.Item key="10" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const ProjectCreateModal = observer(View);
