import { Button, Form, Input, InputNumber, Modal, Select } from "antd";
import { useForm } from "antd/lib/form/Form";
import { UploadImage } from "components/upload-image";
import { observer } from "mobx-react-lite";
import { ProjectPropertyCreateFormValues } from "pages/project-list/classes";
import ProjectListVM from "pages/project-list/vm";
import React, { useEffect } from "react";
import { ProjectPropertyImagePosition } from "utils/constants";

interface Props {
  vm: ProjectListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  useEffect(() => {
    if (vm.propertyItemForUpdate) {
      form.setFieldsValue(vm.propertyItemForUpdate);
    }
  });

  const onFinish = async (values: ProjectPropertyCreateFormValues) => {
    await vm.updateProperty(values);
    form.resetFields();

    vm.closeUpdatePropertyModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeUpdatePropertyModal();
  }

  return (
    <Modal
      title="Редактирование"
      visible={vm.isPropertyUpdateModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          key="1"
          label="Название(RU)"
          name="title_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="2"
          label="Название(EN)"
          name="title_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="3"
          label="Описание(RU)"
          name="description_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="4"
          label="Описание(EN)"
          name="description_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="5"
          label="Позиция картинки"
          name="imagePosition"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Select style={{ width: "100%" }} placeholder="Расположение картинки">
            <Select.Option key={ProjectPropertyImagePosition.LEFT}>
              {vm.getPropertyImagePositionRUS(
                ProjectPropertyImagePosition.LEFT
              )}
            </Select.Option>
            ;
            <Select.Option key={ProjectPropertyImagePosition.RIGHT}>
              {vm.getPropertyImagePositionRUS(
                ProjectPropertyImagePosition.RIGHT
              )}
            </Select.Option>
            ;
            <Select.Option key={ProjectPropertyImagePosition.UP}>
              {vm.getPropertyImagePositionRUS(ProjectPropertyImagePosition.UP)}
            </Select.Option>
            ;
            <Select.Option key={ProjectPropertyImagePosition.DOWN}>
              {vm.getPropertyImagePositionRUS(
                ProjectPropertyImagePosition.DOWN
              )}
            </Select.Option>
            ;
          </Select>
        </Form.Item>

        <Form.Item
          key="6"
          label="Порядоковый номер"
          name="serialNumber"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>

        <Form.Item
          key="7"
          label="Картинка свойства"
          name="image"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item key="8" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const ProjectPropertyUpdateModal = observer(View);
