import { Button, Form, Input, InputNumber, Modal } from "antd";
import { useForm } from "antd/lib/form/Form";
import { ColorPicker } from "components/color-picker";
import { UploadImage } from "components/upload-image";
import { observer } from "mobx-react-lite";
import { ProjectCreateFormValues } from "pages/project-list/classes";
import ProjectListVM from "pages/project-list/vm";
import React, { useEffect } from "react";

interface Props {
  vm: ProjectListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  useEffect(() => {
    if (vm.projectItemForUpdate) {
      form.setFieldsValue(vm.projectItemForUpdate);
    }
  });

  const onSubmit = async (values: ProjectCreateFormValues) => {
    await vm.updateProject(values);
    form.resetFields();

    vm.closeUpdateModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeUpdateModal();
  }

  return (
    <Modal
      title="Редактирование"
      visible={vm.isUpdateModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onSubmit}
        autoComplete="off"
      >
        <Form.Item
          key="1"
          label="Название(RU)"
          name="title_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="2"
          label="Название(EN)"
          name="title_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="3"
          label="Информация о проекте(RU)"
          name="about_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="4"
          label="Информация о проекте(EN)"
          name="about_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="5"
          label="Цвет"
          name="color"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <ColorPicker />
        </Form.Item>

        <Form.Item
          key="6"
          label="Порядоковый номер"
          name="serialNumber"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>

        <Form.Item
          key="7"
          label="Картинка на превью (Logo)"
          name="logo"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item
          key="8"
          label="Картинка на весь экран"
          name="image"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item
          key="9"
          label="Видео"
          name="video"
        >
          <UploadImage />
        </Form.Item>

        <Form.Item key="10" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const ProjectUpdateModal = observer(View);
