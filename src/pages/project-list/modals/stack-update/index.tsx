import { Button, Form, InputNumber, Modal, Select } from "antd";
import { useForm } from "antd/lib/form/Form";
import { observer } from "mobx-react-lite";
import { MemberStackUpdateFormValues } from "pages/member-list/classes";
import ProjectListVM from "pages/project-list/vm";
import React, { useEffect } from "react";

interface Props {
  vm: ProjectListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  useEffect(() => {
    if (vm.stackItemForUpdate) {
      form.setFieldsValue({
        ...vm.stackItemForUpdate,
        serialNumber: vm.stackItemForUpdate.ProjectStack?.serialNumber,
      });
    }
  });

  const onFinish = async (values: MemberStackUpdateFormValues) => {
    await vm.updateStack(values);
    form.resetFields();
    vm.closeUpdateStackModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeUpdateStackModal();
  };

  return (
    <Modal
      title="Обновить элемент стека"
      visible={vm.isStackUpdateModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item key="1" name="stackId" label="Лист стеков">
          <Select disabled style={{ width: "100%" }} placeholder="стек">
            <Select.Option key={vm.stackItemForUpdate?.id}>{vm.stackItemForUpdate?.title_ru}</Select.Option>;
          </Select>
        </Form.Item>

        <Form.Item
          label="Позиция в списке"
          name="serialNumber"
          key="2"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>

        <Form.Item key="3" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const ProjectStackUpdateModal = observer(View);
