import { makeAutoObservable } from "mobx";
import appStore from "stores/app-store";

class LoginVM {
  login: string = "";
  password: string = "";

  constructor() {
    makeAutoObservable(this);
  }

  setLogin = (value: string) => {
    this.login = value;
  };

  setPassword = (value: string) => {
    this.password = value;
  };

  //Действия
  submitLogin = async () => {
    await appStore.login(this.login, this.password);
  };
}

export default LoginVM;
