import { Button, Col, Divider, Input, Row } from "antd";
import { observer } from "mobx-react-lite";
import React from "react";
import styles from "./styles.module.scss";
import LoginVM from "./vm";

interface Props {
  vm: LoginVM;
}

const View = ({ vm }: Props) => {
  const onSubmitLogin = async () => {
    vm.submitLogin();
  };

  return (
    <>
      <Row justify="center" align="middle" className={styles.formContainer}>
        <Col span={8}>
          <Row justify="center">
            <h3 className={styles.title}>Авториация</h3>
          </Row>
          <Row justify="center">
            <h3 className={styles.title}>Введите логин</h3>
          </Row>

          <Input placeholder="login" onChange={(e) => vm.setLogin(e.target.value)} />

          <Divider />

          <Row justify="center">
            <h3 className={styles.title}>Введите пароль</h3>
          </Row>

          <Input placeholder="XXXXXX" onChange={(e) => vm.setPassword(e.target.value)} />

          <Divider />

          <Button block type="primary" htmlType="submit" onClick={onSubmitLogin}>
            Log in
          </Button>
        </Col>
      </Row>
    </>
  );
};

export const LoginScreen = observer(View);
