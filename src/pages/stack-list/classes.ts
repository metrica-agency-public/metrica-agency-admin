import { ResUploadFile } from "api/media/classes";

export interface StackCreateFormValues {
  title_ru: string;
  title_en: string;

  description_ru: string;
  description_en: string;

  image: ResUploadFile;
}
