import { Card as BaseCard, Image, Space } from 'antd';
import { ResStack } from 'api/stack/classes';
import cn from 'classnames';
import { DeleteButton } from 'components/delete-button';
import { EditButton } from 'components/edit-button';
import { confirmDialog } from 'components/modals/SureModal';
import { observer } from 'mobx-react-lite';
import StackListVM from '../vm';
import styles from './styles.module.scss';

interface Props {
  vm: StackListVM;
  item: ResStack;
  className?: string;
}

const View = ({ vm, className, item }: Props) => {
  const onEdit = () => {
    vm.openUpdateModal(item);
  };

  const onDelete = () => {
    confirmDialog({
      onOk: () => {
        vm.deleteStack(item.id);
      },
    });
  };
  return (
    <>
      <BaseCard
        className={cn(styles.root, className)}
        title={item.title_ru}
        extra={
          <Space size="small" direction="horizontal">
            <EditButton onClick={onEdit} />
            <DeleteButton onClick={onDelete} />
          </Space>
        }
      >
        <div className={styles.field}>{'Титул(EN): ' + item.title_en}</div>
        <div className={styles.field}>{'Описание(RU): ' + item.description_ru}</div>
        <div className={styles.field}>{'Описание(EN): ' + item.description_en}</div>
        <div className={styles.field}>{'Картинка: '}</div>
        <Image width={200} src={item.image?.url} />
      </BaseCard>
    </>
  );
};

export const StackListItem = observer(View);
