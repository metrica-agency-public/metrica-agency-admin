import { Button, Input } from 'antd';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { StackListItem } from './list-item';
import { CreateModal } from './modals/create';
import { UpdateModal } from './modals/update';
import styles from './styles.module.scss';
import StackListVM from './vm';

interface Props {
  vm: StackListVM;
}

const View = ({ vm }: Props) => {
  useEffect(() => {
    vm.getStackList();
  }, [vm, vm.existShowType, vm.searchValue]);

  const onSearchChange = (e: any) => {
    vm.setSearchValue(e.target.value);
  };

  const onSearch = () => {
    vm.getStackList();
  };

  return (
    <div className={styles.root}>
      <h3 className={styles.title}>Стек</h3>
      <Input.Search
        value={vm.searchValue}
        onChange={onSearchChange}
        className={styles.search}
        placeholder="Поиск..."
        onSearch={onSearch}
        enterButton
      />
      <div className={styles.buttons}>
        <Button className={styles.button} type="primary" onClick={() => vm.openCreateModal()}>
          Создать стек
        </Button>
      </div>
      <div className={styles.tags}>
        {vm.resStackList?.map((item) => {
          return <StackListItem key={item.id} vm={vm} item={item} className={styles.card} />;
        })}
      </div>
      <CreateModal vm={vm} />
      <UpdateModal vm={vm} />
    </div>
  );
};

export const StackListScreen = observer(View);
