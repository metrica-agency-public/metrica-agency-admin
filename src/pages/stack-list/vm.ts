import { StackAPI } from "api/stack/api";
import { ResStack } from "api/stack/classes";
import { openNotification } from "components/modals/OperationNotification";
import { makeAutoObservable } from "mobx";
import { FilterExistShowType } from "utils/constants";
import { StackCreateFormValues } from "./classes";

class StackListVM {
  resStackList: ResStack[] = [];
  isCreateModal: boolean = false;
  itemForUpdate: ResStack | null = null;
  isUpdateModal: boolean = false;

  //Фильтрация
  existShowType: FilterExistShowType = FilterExistShowType.ALL;
  searchValue: string = "";
  setSearchValue = (value: string) => {
    this.searchValue = value;
  };
  setFilterExistShowType = (existShowType: FilterExistShowType) => {
    this.existShowType = existShowType;
  };

  constructor() {
    makeAutoObservable(this);
  }

  openCreateModal() {
    this.isCreateModal = true;
  }

  closeCreateModal() {
    this.isCreateModal = false;
  }

  openUpdateModal(item: ResStack) {
    this.itemForUpdate = item;
    this.isUpdateModal = true;
  }

  closeUpdateModal() {
    this.itemForUpdate = null;
    this.isUpdateModal = false;
  }

  getStackList = async () => {
    const resp = await StackAPI.getStackList({
      searchValue: this.searchValue ?? "",
      show: this.existShowType,
    });
    if (resp.isSuccess) {
      if (resp.data) {
        this.resStackList = resp.data.stackList;
      }
    }
  };

  createStack = async (values: StackCreateFormValues) => {
    const resp = await StackAPI.createStack({
      ...values,
      imageId: values.image.id,
    });

    if (resp.isSuccess) {
      openNotification("Стек успешно создан");
      this.getStackList();
    }
  };

  updateStack = async (formValues: StackCreateFormValues) => {
    if (!this.itemForUpdate) {
      return;
    }

    const resp = await StackAPI.updateStack(this.itemForUpdate.id, {
      ...formValues,
      imageId: formValues.image.id,
    });

    if (resp.isSuccess) {
      openNotification("Стек успешно обновлен");

      if (resp.data) {
        const id = resp.data.id;
        const index = this.resStackList.findIndex((el) => el.id === id);
        if (index !== -1) {
          this.resStackList.splice(index, 1, resp.data);
        }
      }
    }
  };

  deleteStack = async (id: string) => {
    const resp = await StackAPI.deleteStack(id);
    if (resp.isSuccess) {
      const index = this.resStackList.findIndex((el) => el.id === id);

      if (index !== -1) {
        this.resStackList.splice(index, 1);
      }
    }
  };
}

export default StackListVM;
