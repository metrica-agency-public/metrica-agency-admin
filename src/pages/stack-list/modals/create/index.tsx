import { Button, Form, Input, Modal } from "antd";
import { useForm } from "antd/lib/form/Form";
import { UploadImage } from "components/upload-image";
import { observer } from "mobx-react-lite";
import { StackCreateFormValues } from "pages/stack-list/classes";
import StackListVM from "pages/stack-list/vm";
import React from "react";

interface Props {
  vm: StackListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  const onSubmit = async (values: StackCreateFormValues) => {
    await vm.createStack(values);
    form.resetFields();

    vm.closeCreateModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeCreateModal();
  }

  return (
    <Modal
      title="Добавление"
      visible={vm.isCreateModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onSubmit}
        autoComplete="off"
      >
        <Form.Item
          label="Название стека(RU)"
          name="title_ru"
          key="1"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="2"
          label="Название стека(EN)"
          name="title_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="3"
          label="Описание стека(RU)"
          name="description_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="4"
          label="Описание стека(EN)"
          name="description_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="5"
          label="Картинка"
          name="image"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item key="6" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const CreateModal = observer(View);
