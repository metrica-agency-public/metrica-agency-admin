import { Button, Form, Input, Modal } from "antd";
import { useForm } from "antd/lib/form/Form";
import { UploadImage } from "components/upload-image";
import { observer } from "mobx-react-lite";
import { StackCreateFormValues } from "pages/stack-list/classes";
import StackListVM from "pages/stack-list/vm";
import { useEffect } from "react";

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

interface Props {
  vm: StackListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  useEffect(() => {
    if (vm.itemForUpdate) {
      form.setFieldsValue(vm.itemForUpdate);
    }
  });

  const onSubmit = async (values: StackCreateFormValues) => {
    await vm.updateStack(values);
    form.resetFields();

    vm.closeUpdateModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeUpdateModal();
  }

  return (
    <Modal
      title="Редактирование"
      visible={vm.isUpdateModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form name="billiard" onFinish={onSubmit} form={form} {...formItemLayout}>
        <Form.Item
          key="1"
          name="title_ru"
          label="Название стека(RU)"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          key="2"
          name="title_en"
          label="Название стека(EN)"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="3"
          name="description_ru"
          label="Описание стека(RU)"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="4"
          name="description_en"
          label="Описание стека(EN)"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="5"
          label="Картинка"
          name="image"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const UpdateModal = observer(View);
