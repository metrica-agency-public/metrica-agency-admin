import styles from "./styles.module.scss";

export const Loader = () => {
  return (
    <div className={styles.container}>
      <h2>Пожалуйста, подождите...</h2>
    </div>
  );
};