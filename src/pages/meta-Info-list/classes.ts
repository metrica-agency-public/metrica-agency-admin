export interface MetaInfoCreateFormValues {
    title_ru: string;
    description_ru: string;

    title_en: string;
    description_en: string;

    serialNumber: number;
  }