import { Button, Form, Input, InputNumber, Modal } from "antd";
import { useForm } from "antd/lib/form/Form";
import { observer } from "mobx-react-lite";
import { MetaInfoCreateFormValues } from "pages/meta-Info-list/classes";
import MetaInfoVM from "pages/meta-Info-list/vm";
import React, { useEffect } from "react";

interface Props {
  vm: MetaInfoVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  useEffect(() => {
    if (vm.itemForUpdate) {
      form.setFieldsValue(vm.itemForUpdate);
    }
  });

  const onFinish = async (values: MetaInfoCreateFormValues) => {
    await vm.updateMetaInfo(values);
    form.resetFields();
    vm.closeUpdateModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeUpdateModal();
  };

  return (
    <Modal
      title="Обновление"
      visible={vm.isUpdateModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          key="1"
          label="Заголовок(RU)"
          name="title_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="2"
          label="Заголовок(EN)"
          name="title_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="3"
          label="Описание(RU)"
          name="description_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="4"
          label="Описание(EN)"
          name="description_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="5"
          label="Порядковый номер"
          name="serialNumber"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>

        <Form.Item key="6" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const UpdateModal = observer(View);
