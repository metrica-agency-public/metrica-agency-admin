import { MetaInfoAPI } from 'api/meta-info/api';
import { ResMetaInfo } from 'api/meta-info/classes';
import { makeAutoObservable } from 'mobx';
import { MetaInfoCreateFormValues } from './classes';

class MetaInfoVM {
  metaInfoList: ResMetaInfo[] = [];
  isCreateModal: boolean = false;
  isUpdateModal: boolean = false;
  itemForUpdate: ResMetaInfo | null = null;
  searchValue: string = '';

  constructor() {
    makeAutoObservable(this);
  }

  openCreateModal() {
    this.isCreateModal = true;
  }

  closeCreateModal() {
    this.isCreateModal = false;
  }

  openUpdateModal(item: ResMetaInfo) {
    this.itemForUpdate = item;
    this.isUpdateModal = true;
  }

  closeUpdateModal() {
    this.isUpdateModal = false;
    this.itemForUpdate = null;
  }

  setSearchValue = (value: string) => {
    this.searchValue = value;
  };

  getMetaInfoList = async () => {
    const resp = await MetaInfoAPI.getMetaInfoList({
      searchValue: this.searchValue ?? '',
    });

    if (resp.isSuccess) {
      if (resp.data) {
        this.metaInfoList = resp.data.metaInfoList;
      }
    }
  };

  createMetaInfo = async (values: MetaInfoCreateFormValues) => {
    const resp = await MetaInfoAPI.addMetaInfo(values);

    if (resp.isSuccess) {
      this.getMetaInfoList();
    }
  };

  updateMetaInfo = async (values: MetaInfoCreateFormValues) => {
    if (!this.itemForUpdate) {
      return;
    }

    const resp = await MetaInfoAPI.patchMetaInfo(this.itemForUpdate.id, values);
    if (resp.isSuccess) {
      this.getMetaInfoList();
    }
  };

  deleteMetaInfo = async (id: string) => {
    const resp = await MetaInfoAPI.deleteMetaInfo(id);
    if (resp.isSuccess) {
      this.getMetaInfoList();
    }
  };
}

export default MetaInfoVM;
