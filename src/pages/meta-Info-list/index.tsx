import { Button, Input } from "antd";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { MetaInfoListItem } from "./list-item";
import { CreateModal } from "./modals/create";
import { UpdateModal } from "./modals/update";
import styles from "./styles.module.scss";
import MetaInfoVM from "./vm";

interface Props {
  vm: MetaInfoVM;
}

const View = ({ vm }: Props) => {
  useEffect(() => {
    vm.getMetaInfoList();
  }, [vm, vm.searchValue]);

  const onSearchChange = (e: any) => {
    vm.setSearchValue(e.target.value);
  };

  const onSearch = () => {
    vm.getMetaInfoList();
  };

  return (
    <div className={styles.root}>
      <h3 className={styles.title}>Мета-инфо</h3>
      <Input.Search
        value={vm.searchValue}
        onChange={onSearchChange}
        className={styles.search}
        placeholder="Поиск..."
        onSearch={onSearch}
        enterButton
      />
      <div className={styles.buttons}>
        <Button className={styles.button} type="primary" onClick={() => vm.openCreateModal()}>
          Создать мета-инфо
        </Button>
      </div>
      <div className={styles.tags}>
        {vm.metaInfoList?.map((item) => {
          return <MetaInfoListItem key={item.id} vm={vm} item={item} />;
        })}
      </div>
      <CreateModal vm={vm} />
      <UpdateModal vm={vm} />
    </div>
  );
};

export const MetaInfoListScreen = observer(View);
