import { Card as BaseCard, Space } from "antd";
import { ResMetaInfo } from "api/meta-info/classes";
import cn from "classnames";
import { DeleteButton } from "components/delete-button";
import { EditButton } from "components/edit-button";
import { confirmDialog } from "components/modals/SureModal";
import { observer } from "mobx-react-lite";
import MetaInfoVM from "../vm";
import styles from "./styles.module.scss";

interface Props {
  vm: MetaInfoVM;
  item: ResMetaInfo;
}

const View = ({ vm, item }: Props) => {
  const onEdit = () => {
    vm.openUpdateModal(item);
  };

  const onDelete = () => {
    confirmDialog({
      onOk: () => {
        vm.deleteMetaInfo(item.id);
      },
    });
  };

  return (
    <>
      <BaseCard
        className={cn(styles.root, styles.card)}
        title={item.title_ru}
        extra={
          <Space size="small" direction="horizontal">
            <EditButton onClick={onEdit} />
            <DeleteButton onClick={onDelete} />
          </Space>
        }
      >
        <div className={styles.field}>{"Заголовок(EN)" + item.title_en}</div>
        <div className={styles.field}>{"Описание(RU): " + item.description_ru}</div>
        <div className={styles.field}>{"Описание(EN): " + item.description_en}</div>
        <div className={styles.field}>{"Порядковый номер: " + item.serialNumber}</div>
      </BaseCard>
    </>
  );
};

export const MetaInfoListItem = observer(View);
