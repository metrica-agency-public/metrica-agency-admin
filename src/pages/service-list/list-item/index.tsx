import { Button, Card as BaseCard, Image, Space } from 'antd';
import { ResService } from 'api/services/classes';
import cn from 'classnames';
import { DeleteButton } from 'components/delete-button';
import { EditButton } from 'components/edit-button';
import { confirmDialog } from 'components/modals/SureModal';
import { observer } from 'mobx-react-lite';
import ServiceListVM from '../vm';
import styles from './styles.module.scss';

interface Props {
  vm: ServiceListVM;
  item: ResService;
  className?: string;
}

const View = ({ vm, className, item }: Props) => {
  const onEdit = () => {
    vm.openUpdateModal(item);
  };

  const onDelete = () => {
    confirmDialog({
      onOk: () => {
        vm.deleteService(item.id);
      },
    });
  };

  const onDeleteForever = () => {
    confirmDialog({
      onOk: () => {
        vm.deleteServiceForever(item.id);
      },
    });
  };

  const onRestore = () => {
    confirmDialog({
      onOk: () => {
        vm.restoreService(item.id);
      },
    });
  };

  return (
    <>
      <BaseCard
        className={cn(styles.root, className)}
        title={item.title_ru}
        extra={
          <>
            {item.isExist && (
              <Space size="small" direction="horizontal">
                <EditButton onClick={onEdit} />
                <DeleteButton onClick={onDelete} />
              </Space>
            )}
            {!item.isExist && (
              <Space size="small" direction="horizontal">
                <Button type="primary" onClick={onRestore}>
                  Восстановить
                </Button>
                <DeleteButton text="Удалить навсегда" onClick={onDeleteForever} />
              </Space>
            )}
          </>
        }
      >
        <div className={styles.field}>{'Тайтл(EN): ' + item.title_en}</div>
        <div className={styles.field}>{'Описание(RU): ' + item.about_ru}</div>
        <div className={styles.field}>{'Описание(EN): ' + item.about_en}</div>
        <div className={styles.field}>{'Порядковый номер: ' + item.serialNumber}</div>
        <div className={styles.field}>{'Отображается?: ' + item.isExist}</div>
        <div className={styles.field}>{'Картинка: '}</div>
        <Image width={200} src={item.image?.url} />
      </BaseCard>
    </>
  );
};

export const ServiceListItem = observer(View);
