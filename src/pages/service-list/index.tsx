import { Button, Input, Select } from 'antd';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { FilterExistShowType } from 'utils/constants';
import { formatShowTypeRUS } from 'utils/helpers';
import { ServiceListItem } from './list-item';
import { ServiceCreateModal } from './modals/create';
import { ServiceUpdateModal } from './modals/update';
import styles from './styles.module.scss';
import ServiceListVM from './vm';

interface Props {
  vm: ServiceListVM;
}

const View = ({ vm }: Props) => {
  useEffect(() => {
    vm.getServiceList();
  }, [vm, vm.searchValue, vm.existShowType]);

  const onSearchChange = (e: any) => {
    vm.setSearchValue(e.target.value);
  };

  const onSearch = () => {
    vm.getServiceList();
  };

  const onExistShowChange = (value: FilterExistShowType) => {
    vm.setFilterExistShowType(value);
  };

  return (
    <div className={styles.root}>
      <h3 className={styles.title}>Услуги</h3>
      <Input.Search
        value={vm.searchValue}
        onChange={onSearchChange}
        className={styles.search}
        placeholder="Поиск..."
        onSearch={onSearch}
        enterButton
      />
      <div className={styles.filterBlock}>
        <div className={styles.filterTitle}>Фильтрация</div>
        <div className={styles.row}>
          <div className={styles.filter}>
            По статусу:
            <Select value={vm.existShowType} style={{ width: 160 }} onChange={onExistShowChange}>
              <Select.Option value={FilterExistShowType.ALL}>{formatShowTypeRUS(FilterExistShowType.ALL)}</Select.Option>
              <Select.Option value={FilterExistShowType.EXISTING}>{formatShowTypeRUS(FilterExistShowType.EXISTING)}</Select.Option>
              <Select.Option value={FilterExistShowType.DELETED}>{formatShowTypeRUS(FilterExistShowType.DELETED)}</Select.Option>
            </Select>
          </div>
        </div>
      </div>
      <div className={styles.buttons}>
        <Button className={styles.button} type="primary" onClick={() => vm.openCreateModal()}>
          Создать услугу
        </Button>
      </div>
      <div className={styles.tags}>
        {vm.resServiceList?.map((item) => {
          return <ServiceListItem key={item.id} vm={vm} item={item} className={styles.card} />;
        })}
      </div>
      <ServiceCreateModal vm={vm} />
      <ServiceUpdateModal vm={vm} />
    </div>
  );
};

export const ServiceListScreen = observer(View);
