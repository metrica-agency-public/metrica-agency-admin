import { ServiceAPI } from "api/services/api";
import { ResService } from "api/services/classes";
import { openNotification } from "components/modals/OperationNotification";
import { makeAutoObservable } from "mobx";
import { FilterExistShowType } from "utils/constants";
import { ServiceCreateFormValues } from "./classes";

class ServiceListVM {
  resServiceList: ResService[] = [];
  isCreateModal: boolean = false;
  itemForUpdate: ResService | null = null;
  isUpdateModal: boolean = false;

  existShowType: FilterExistShowType = FilterExistShowType.ALL;
  searchValue: string = "";

  constructor() {
    makeAutoObservable(this);
  }

  setSearchValue = (value: string) => {
    this.searchValue = value;
  };
  setFilterExistShowType = (existShowType: FilterExistShowType) => {
    this.existShowType = existShowType;
  };

  openCreateModal() {
    this.isCreateModal = true;
  }

  closeCreateModal() {
    this.isCreateModal = false;
  }

  openUpdateModal(item: ResService) {
    this.itemForUpdate = item;
    this.isUpdateModal = true;
  }

  closeUpdateModal() {
    this.itemForUpdate = null;
    this.isUpdateModal = false;
  }

  getServiceList = async () => {
    const resp = await ServiceAPI.getServices({
      searchValue: this.searchValue ?? "",
      show: this.existShowType,
    });
    if (resp.isSuccess) {
      if (resp.data) {
        this.resServiceList = resp.data.serviceList;
      }
    }
  };

  createService = async (formValues: ServiceCreateFormValues) => {
    const resp = await ServiceAPI.createService({
      ...formValues,
      imageId: formValues.image.id,
    });

    if (resp.isSuccess) {
      openNotification("Сервис успешно создан");
      this.getServiceList();
    }
  };

  updateService = async (formValues: ServiceCreateFormValues) => {
    if (!this.itemForUpdate) {
      return;
    }

    const resp = await ServiceAPI.patchService(this.itemForUpdate.id, {
      ...formValues,
      imageId: formValues.image.id,
    });

    if (resp.isSuccess) {
      openNotification("Сервис успешно обновлен");
      if (resp.data) {
        this.updateServiceById(resp.data);
      }
    }
  };

  restoreService = async (id: string) => {
    const resp = await ServiceAPI.restoreService(id);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateServiceById(resp.data);
      }
    }
  };

  deleteService = async (id: string) => {
    const resp = await ServiceAPI.deleteService(id);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateServiceById(resp.data);
      }
    }
  };

  deleteServiceForever = async (id: string) => {
    const resp = await ServiceAPI.deleteServiceForever(id);
    if (resp.isSuccess) {
      const index = this.resServiceList.findIndex((el) => el.id === id);
      if (index !== -1) {
        this.resServiceList.splice(index, 1);
      }
    }
  };

  updateServiceById(serviceItem: ResService) {
    const index = this.resServiceList.findIndex((el) => el.id === serviceItem.id);
    if (index !== -1) {
      this.resServiceList.splice(index, 1, serviceItem);
    }
  }
}

export default ServiceListVM;
