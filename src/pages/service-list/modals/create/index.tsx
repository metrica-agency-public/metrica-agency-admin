import { Button, Form, Input, InputNumber, Modal } from "antd";
import { useForm } from "antd/lib/form/Form";
import { UploadImage } from "components/upload-image";
import { observer } from "mobx-react-lite";
import { ServiceCreateFormValues } from "pages/service-list/classes";
import ServiceListVM from "pages/service-list/vm";
import React from "react";

interface Props {
  vm: ServiceListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  const onFinish = async (values: ServiceCreateFormValues) => {
    await vm.createService(values);
    form.resetFields();

    vm.closeCreateModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeCreateModal();
  }

  return (
    <Modal
      title="Добавление"
      visible={vm.isCreateModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form
        form={form}
        name="basic"
        key="0"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        // initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Название услуги(RU)"
          name="title_ru"
          key="1"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Название услуги(EN)"
          name="title_en"
          key="2"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Описание услуги(RU)"
          name="about_ru"
          key="3"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Описание услуги(EN)"
          name="about_en"
          key="4"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Порядковый номер услуги"
          name="serialNumber"
          key="5"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>

        <Form.Item
          key="6"
          label="Картинка"
          name="image"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item key="7" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const ServiceCreateModal = observer(View);
