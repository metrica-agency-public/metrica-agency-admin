import { Button, Form, Input, InputNumber, Modal } from "antd";
import { useForm } from "antd/lib/form/Form";
import { UploadImage } from "components/upload-image";
import { observer } from "mobx-react-lite";
import { ServiceCreateFormValues } from "pages/service-list/classes";
import ServiceListVM from "pages/service-list/vm";
import { useEffect } from "react";

interface Props {
  vm: ServiceListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  useEffect(() => {
    if (vm.itemForUpdate) {
      form.setFieldsValue(vm.itemForUpdate);
    }
  });

  const onFinish = async (values: ServiceCreateFormValues) => {
    await vm.updateService(values);
    form.resetFields();
    vm.closeUpdateModal();
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeUpdateModal();
  }

  return (
    <Modal
      title="Редактирование"
      visible={vm.isUpdateModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form name="billiard" onFinish={onFinish} form={form} {...formItemLayout}>
        <Form.Item
          key="1"
          name="title_ru"
          label="Название услуги(RU)"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="2"
          name="title_en"
          label="Название услуги(EN)"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="3"
          name="about_ru"
          label="Описание услуги(RU)"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="4"
          name="about_en"
          label="Описание услуги(EN)"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="5"
          name="serialNumber"
          label="Порядковый номер услуги"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>

        <Form.Item
          key="6"
          label="Картинка"
          name="image"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item key="7">
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const ServiceUpdateModal = observer(View);
