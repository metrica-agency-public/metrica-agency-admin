import { ResUploadFile } from "api/media/classes";

export interface ServiceCreateFormValues {
  title_ru: string;
  title_en: string;

  about_ru: string;
  about_en: string;
  
  serialNumber: number;

  image: ResUploadFile;
}
