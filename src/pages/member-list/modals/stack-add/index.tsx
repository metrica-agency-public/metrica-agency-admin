import { Button, Form, InputNumber, Modal, Select } from "antd";
import { useForm } from "antd/lib/form/Form";
import { observer } from "mobx-react-lite";
import { MemberStackCreateFormValues } from "pages/member-list/classes";
import MemberListVM from "pages/member-list/vm";
import React, { useEffect } from "react";

interface Props {
  vm: MemberListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  useEffect(() => {
    if (vm.isStackAddModal) {
      vm.getStackList();
    }
  }, [vm, vm.isStackAddModal]);

  const onFinish = async (values: MemberStackCreateFormValues) => {
    await vm.addStack(values);
    form.resetFields();

    vm.closeAddStackModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeAddStackModal();
  };

  return (
    <Modal
      title="Добавить элемент стека"
      visible={vm.isStackAddModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item key="1" name="stackId" label="Лист стекова">
          <Select style={{ width: "100%" }} placeholder="стек">
            {vm.getFilterStackList().map((stack) => {
              return (
                <Select.Option key={stack.id}>{stack.title_ru}</Select.Option>
              );
            })}
          </Select>
        </Form.Item>

        <Form.Item
          label="Позиция в списке"
          name="serialNumber"
          key="2"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>

        <Form.Item key="3" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const MemberStackAddModal = observer(View);
