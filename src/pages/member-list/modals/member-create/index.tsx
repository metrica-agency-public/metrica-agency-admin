import { Button, Form, Input, InputNumber, Modal, Select } from "antd";
import { useForm } from "antd/lib/form/Form";
import { UploadImage } from "components/upload-image";
import { observer } from "mobx-react-lite";
import { MemberCreateFormValues } from "pages/member-list/classes";
import MemberListVM from "pages/member-list/vm";
import React from "react";
import { MemberGroup } from "utils/constants";

interface Props {
  vm: MemberListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  const onFinish = async (values: MemberCreateFormValues) => {
    await vm.createMember(values);
    form.resetFields();

    vm.closeCreateModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeCreateModal();
  };

  return (
    <Modal title="Добавление" visible={vm.isCreateModal} onCancel={onCancel} footer={false} width={800}>
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Имя(RU)"
          name="firstName_ru"
          key="1"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Имя(EN)"
          name="firstName_en"
          key="2"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="3"
          label="Фамилия(RU)"
          name="lastName_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="4"
          label="Фамилия(EN)"
          name="lastName_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="5"
          label="О себе(RU)"
          name="about_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item
          key="6"
          label="О себе(EN)"
          name="about_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item
          key="7"
          label="Скилы(RU)"
          name="skills_ru"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="8"
          label="Скилы(EN)"
          name="skills_en"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Роль в группе"
          name="group"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Select style={{ width: "100%" }} placeholder="Роль в группе">
            <Select.Option key={MemberGroup.MAINTAINER}>{MemberGroup.MAINTAINER}</Select.Option>
            <Select.Option key={MemberGroup.DEVELOPER}>{MemberGroup.DEVELOPER}</Select.Option>
            <Select.Option key={MemberGroup.DESIGNER}>{MemberGroup.DESIGNER}</Select.Option>
            <Select.Option key={MemberGroup.ANALYTICS_AND_QA}>{MemberGroup.ANALYTICS_AND_QA}</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item
          key="9"
          label="Порядоковый номер"
          name="serialNumber"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>

        <Form.Item
          key="10"
          label="Картинка на превью"
          name="previewImage"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item
          key="11"
          label="Картинка на весь экран"
          name="fullImage"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <UploadImage />
        </Form.Item>

        <Form.Item key="12" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const MemberCreateModal = observer(View);
