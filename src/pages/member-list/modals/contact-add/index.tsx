import { Button, Form, Input, InputNumber, Modal, Select } from "antd";
import { useForm } from "antd/lib/form/Form";
import { observer } from "mobx-react-lite";
import { MemberContactCreateFormValues } from "pages/member-list/classes";
import MemberListVM from "pages/member-list/vm";
import React from "react";
import { MemberContactType } from "utils/constants";

interface Props {
  vm: MemberListVM;
}

const View = ({ vm }: Props) => {
  const [form] = useForm();

  const onFinish = async (values: MemberContactCreateFormValues) => {
    await vm.addContact(values);
    form.resetFields();

    vm.closeAddContactModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeAddContactModal();
  };

  return (
    <Modal
      title="Добавить контакт участнику"
      visible={vm.isContactAddModal}
      onCancel={onCancel}
      footer={false}
      width={800}
    >
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          key="1"
          initialValue={MemberContactType.HH}
          name="type"
          label="Тип контакта"
        >
          <Select style={{ width: "100%" }} placeholder="Тип контакта">
            <Select.Option key={MemberContactType.HH}>
              {MemberContactType.HH}
            </Select.Option>
            ;
            <Select.Option key={MemberContactType.TELEGRAM}>
              {MemberContactType.TELEGRAM}
            </Select.Option>
            ;
            <Select.Option key={MemberContactType.WHATSAPP}>
              {MemberContactType.WHATSAPP}
            </Select.Option>
            ;
            <Select.Option key={MemberContactType.PHONE}>
              {MemberContactType.PHONE}
            </Select.Option>
            ;
            <Select.Option key={MemberContactType.GIT}>
              {MemberContactType.GIT}
            </Select.Option>
            ;
          </Select>
        </Form.Item>

        <Form.Item
          label="Телефон или ссылка"
          name="value"
          key="2"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Позиция в списке"
          name="serialNumber"
          key="3"
          rules={[
            {
              required: true,
              message: "Field is required",
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>

        <Form.Item key="4" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const MemberContactAddModal = observer(View);
