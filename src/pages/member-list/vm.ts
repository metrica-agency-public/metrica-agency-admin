import { MembersAPI } from "api/members/api";
import { ResMember, ResMemberContact } from "api/members/classes";
import { StackAPI } from "api/stack/api";
import { ResStack } from "api/stack/classes";
import { openNotification } from "components/modals/OperationNotification";
import { makeAutoObservable } from "mobx";
import { FilterExistShowType } from "utils/constants";
import {
  MemberContactCreateFormValues,
  MemberCreateFormValues,
  MemberStackCreateFormValues,
  MemberStackUpdateFormValues,
} from "./classes";

class MemberListVM {
  memberList: ResMember[] = [];
  isCreateModal: boolean = false;
  memberItemForUpdate: ResMember | null = null;
  isUpdateModal: boolean = false;

  existShowType: FilterExistShowType = FilterExistShowType.ALL;
  searchValue: string = "";

  //Работа со стеком
  resStackList: ResStack[] = []; //Список стека который пришел с сервера
  isStackAddModal: boolean = false;
  isStackUpdateModal: boolean = false;
  stackItemForUpdate: ResStack | null = null; //Элемент для обновления стека
  stackIdAdd: string | null = null;

  //Работа с контактами
  isContactAddModal: boolean = false;
  isContactUpdateModal: boolean = false;
  contactItemForUpdate: ResMemberContact | null = null;

  constructor() {
    makeAutoObservable(this);
  }

  setSearchValue = (value: string) => {
    this.searchValue = value;
  };
  setFilterExistShowType = (existShowType: FilterExistShowType) => {
    this.existShowType = existShowType;
  };

  setStackIdAdd(value: string) {
    this.stackIdAdd = value;
  }

  //Модалки для работы с Членами команды
  openCreateModal() {
    this.isCreateModal = true;
  }

  closeCreateModal() {
    this.isCreateModal = false;
  }

  openUpdateModal(item: ResMember) {
    this.memberItemForUpdate = item;
    this.isUpdateModal = true;
  }

  closeUpdateModal() {
    this.isUpdateModal = false;
    this.memberItemForUpdate = null;
  }

  //Модалки для добавления и обновления стека у члена команды
  openAddStackModal(memberItem: ResMember) {
    this.memberItemForUpdate = memberItem;
    this.stackIdAdd = null;
    this.isStackAddModal = true;
  }

  closeAddStackModal() {
    this.isStackAddModal = false;
    this.memberItemForUpdate = null;
    this.stackIdAdd = null;
  }

  openUpdateStackModal(memberItem: ResMember, stackItem: ResStack) {
    this.memberItemForUpdate = memberItem;
    this.stackItemForUpdate = stackItem;
    this.isStackUpdateModal = true;
  }

  closeUpdateStackModal() {
    this.isStackUpdateModal = false;
    this.memberItemForUpdate = null;
    this.stackItemForUpdate = null;
  }

  //Модалки для добавления и редактирования контактов
  openAddContactModal(memberItem: ResMember) {
    this.memberItemForUpdate = memberItem;
    this.isContactAddModal = true;
  }

  closeAddContactModal() {
    this.isContactAddModal = false;
    this.memberItemForUpdate = null;
  }

  openUpdateContactModal(memberItem: ResMember, contactItem: ResMemberContact) {
    this.memberItemForUpdate = memberItem;
    this.contactItemForUpdate = contactItem;
    this.isContactUpdateModal = true;
  }

  closeUpdateContactModal() {
    this.isContactUpdateModal = false;
    this.memberItemForUpdate = null;
    this.contactItemForUpdate = null;
  }

  //Действия
  getStackList = async () => {
    const resp = await StackAPI.getStackList();
    if (resp.isSuccess) {
      if (resp.data) {
        this.resStackList = resp.data?.stackList;
      }
    }
  };

  getMemberList = async () => {
    const resp = await MembersAPI.getMemberList({
      searchValue: this.searchValue ?? "",
      show: this.existShowType,
    });
    if (resp.isSuccess) {
      if (resp.data) {
        this.memberList = resp.data?.memberList;
      }
    }
  };

  getFilterStackList = () => {
    let result: ResStack[] = [];
    if (this.memberItemForUpdate && this.resStackList) {
      this.resStackList.forEach((stack) => {
        const index = this.memberItemForUpdate!.stackList.findIndex(
          (el) => el.id === stack.id
        );
        if (index === -1) {
          result.push(stack);
        }
      });
    }
    return result;
  };


  createMember = async (values: MemberCreateFormValues) => {
    const resp = await MembersAPI.createMember({
      ...values,
      previewImageId: values.previewImage.id,
      fullImageId: values.fullImage.id,
    });

    if (resp.isSuccess) {
      openNotification("Участник успешно создан");
      this.getMemberList();
    }
  };

  updateMember = async (values: MemberCreateFormValues) => {
    if (!this.memberItemForUpdate) return;

    const resp = await MembersAPI.updateMember(this.memberItemForUpdate.id, {
      ...values,
      previewImageId: values.previewImage.id,
      fullImageId: values.fullImage.id,
    });

    if (resp.isSuccess) {
      openNotification("Участник успешно обновлен");
      if (resp.data) {
        this.updateMemberById(resp.data);
      }
    }
  };

  //Работа с членами команды
  restoreMember = async (id: string) => {
    const resp = await MembersAPI.restoreMember(id);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateMemberById(resp.data);
      }
    }
  };

  deleteMember = async (id: string) => {
    const resp = await MembersAPI.deleteMember(id);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateMemberById(resp.data);
      }
    }
  };

  deleteMemberForever = async (id: string) => {
    const resp = await MembersAPI.deleteMemberForever(id);
    if (resp.isSuccess) {
      const index = this.memberList.findIndex((el) => el.id === id);
      if (index !== -1) {
        this.memberList.splice(index, 1);
      }
    }
  };

  updateMemberById(memberItem: ResMember) {
    const index = this.memberList.findIndex((el) => el.id === memberItem.id);
    if (index !== -1) {
      this.memberList.splice(index, 1, memberItem);
    }
  }

  //Работа со стеком
  addStack = async (values: MemberStackCreateFormValues) => {
    if (!this.memberItemForUpdate) return;

    const resp = await MembersAPI.addStack(this.memberItemForUpdate.id, values);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateMemberById(resp.data);
      }
    }
  };

  updateStack = async (values: MemberStackUpdateFormValues) => {
    if (!this.memberItemForUpdate || !this.stackItemForUpdate) return;

    const resp = await MembersAPI.updateStack(this.memberItemForUpdate.id, this.stackItemForUpdate.id, values);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateMemberById(resp.data);
      }
    }
  };

  deleteStack = async (memberId: string, stackId: string) => {
    const resp = await MembersAPI.deleteStack(memberId, stackId);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateMemberById(resp.data);
      }
    }
  };

  //Работа с контактами
  addContact = async (values: MemberContactCreateFormValues) => {
    if (!this.memberItemForUpdate) return;

    const resp = await MembersAPI.addContact(this.memberItemForUpdate.id, values);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateMemberById(resp.data);
      }
    }
  };

  updateContact = async (values: MemberContactCreateFormValues) => {
    if (!this.memberItemForUpdate || !this.contactItemForUpdate) return;

    const resp = await MembersAPI.updateContact(this.memberItemForUpdate.id, this.contactItemForUpdate.id, values);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateMemberById(resp.data);
      }
    }
  };

  deleteContact = async (memberId: string, contactId: string) => {
    const resp = await MembersAPI.deleteContact(memberId, contactId);
    if (resp.isSuccess) {
      if (resp.data) {
        this.updateMemberById(resp.data);
      }
    }
  };
}

export default MemberListVM;
