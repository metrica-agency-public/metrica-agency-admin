import { ResUploadFile } from "api/media/classes";

export interface MemberCreateFormValues {
  firstName_ru: string;
  lastName_ru: string;
  about_ru: string;
  skills_ru: string; // DevOps,TeamLead, ...

  firstName_en: string;
  lastName_en: string;
  about_en: string;
  skills_en: string; // DevOps,TeamLead, ...

  serialNumber: number; // порядковый номер в списке

  fullImage: ResUploadFile;
  previewImage: ResUploadFile;
}

//Работа со стеком
export interface MemberStackCreateFormValues {
  stackId: string;
  serialNumber: number;
}

export interface MemberStackUpdateFormValues {
  serialNumber: number;
}

//Работа с контактами
export interface MemberContactCreateFormValues {
  type: string;
  value: string;
  serialNumber: number;
}
