import { Button, Input, Select } from "antd";
import { observer } from "mobx-react-lite";
import React, { useEffect } from "react";
import { FilterExistShowType } from "utils/constants";
import { formatShowTypeRUS } from "utils/helpers";
import { MemberListItem } from "./list-item";
import { MemberContactAddModal } from "./modals/contact-add";
import { MemberContactUpdateModal } from "./modals/contact-update";
import { MemberCreateModal } from "./modals/member-create";
import { MemberUpdateModal } from "./modals/member-update";
import { MemberStackAddModal } from "./modals/stack-add";
import { MemberStackUpdateModal } from "./modals/stack-update";
import styles from "./styles.module.scss";
import MemberListVM from "./vm";

interface Props {
  vm: MemberListVM;
}

const View = ({ vm }: Props) => {
  useEffect(() => {
    vm.getMemberList();
  }, [vm, vm.existShowType, vm.searchValue]);

  const onSearchChange = (e: any) => {
    vm.setSearchValue(e.target.value);
  };

  const onSearch = () => {
    vm.getStackList();
  };

  const onExistShowChange = (value: FilterExistShowType) => {
    vm.setFilterExistShowType(value);
  };

  return (
    <div className={styles.root}>
      <h3 className={styles.title}>Члены комманды</h3>
      <Input.Search
        value={vm.searchValue}
        onChange={onSearchChange}
        className={styles.search}
        placeholder="Поиск..."
        onSearch={onSearch}
        enterButton
      />
      <div className={styles.filterBlock}>
        <div className={styles.filterTitle}>Фильтрация</div>
        <div className={styles.row}>
          <div className={styles.filter}>
            По статусу:
            <Select value={vm.existShowType} style={{ width: 160 }} onChange={onExistShowChange}>
              <Select.Option value={FilterExistShowType.ALL}>
                {formatShowTypeRUS(FilterExistShowType.ALL)}
              </Select.Option>
              <Select.Option value={FilterExistShowType.EXISTING}>
                {formatShowTypeRUS(FilterExistShowType.EXISTING)}
              </Select.Option>
              <Select.Option value={FilterExistShowType.DELETED}>
                {formatShowTypeRUS(FilterExistShowType.DELETED)}
              </Select.Option>
            </Select>
          </div>
        </div>
      </div>
      <div className={styles.buttons}>
        <Button className={styles.button} type="primary" onClick={() => vm.openCreateModal()}>
          Создать участника
        </Button>
      </div>
      <div className={styles.tags}>
        {vm.memberList?.map((item) => {
          return <MemberListItem key={item.id} vm={vm} item={item} className={styles.card} />;
        })}
      </div>
      <MemberCreateModal vm={vm} />
      <MemberUpdateModal vm={vm} />

      <MemberStackAddModal vm={vm} />
      <MemberStackUpdateModal vm={vm} />

      <MemberContactAddModal vm={vm} />
      <MemberContactUpdateModal vm={vm} />
    </div>
  );
};

export const MemberListScreen = observer(View);
