import { Button, Card as BaseCard, Collapse, Image, Space } from 'antd';
import { ResMember, ResMemberContact } from 'api/members/classes';
import { ResStack } from 'api/stack/classes';
import cn from 'classnames';
import { DeleteButton } from 'components/delete-button';
import { EditButton } from 'components/edit-button';
import { confirmDialog } from 'components/modals/SureModal';
import { observer } from 'mobx-react-lite';
import MemberListVM from '../vm';
import styles from './styles.module.scss';

interface Props {
  vm: MemberListVM;
  item: ResMember;
  className?: string;
}

const View = ({ vm, className, item }: Props) => {
  const onEdit = () => {
    vm.openUpdateModal(item);
  };

  const onDelete = () => {
    confirmDialog({
      onOk: () => {
        vm.deleteMember(item.id);
      },
    });
  };

  const onDeleteForever = () => {
    confirmDialog({
      onOk: () => {
        vm.deleteMemberForever(item.id);
      },
    });
  };

  const onRestore = () => {
    confirmDialog({
      onOk: () => {
        vm.restoreMember(item.id);
      },
    });
  };

  //Работа со стеком
  const onEditStack = (stack: ResStack) => {
    vm.openUpdateStackModal(item, stack);
  };

  const onDeleteStack = (stackId: string) => {
    confirmDialog({
      onOk: () => {
        vm.deleteStack(item.id, stackId);
      },
    });
  };

  //Работа с контактами
  const onEditContact = (resContact: ResMemberContact) => {
    vm.openUpdateContactModal(item, resContact);
  };

  const onDeleteContact = (contactId: string) => {
    confirmDialog({
      onOk: () => {
        vm.deleteContact(item.id, contactId);
      },
    });
  };

  return (
    <>
      <BaseCard
        className={cn(styles.root, className)}
        title={item.firstName_ru + ' ' + item.lastName_ru}
        extra={
          <>
            {item.isExist && (
              <Space size="small" direction="horizontal">
                <EditButton onClick={onEdit} />
                <DeleteButton onClick={onDelete} />
              </Space>
            )}
            {!item.isExist && (
              <Space size="small" direction="horizontal">
                <Button type="primary" onClick={onRestore}>
                  Восстановить
                </Button>
                <DeleteButton text="Удалить навсегда" onClick={onDeleteForever} />
              </Space>
            )}
          </>
        }
      >
        <div className={styles.field}>{'Тайтл(EN): ' + item.firstName_en + ' ' + item.lastName_en}</div>
        <div className={styles.field}>{'Описание(RU): ' + item.about_ru}</div>
        <div className={styles.field}>{'Скилы(RU): ' + item.skills_ru}</div>
        <div className={styles.field}>{'Описание(EN): ' + item.about_en}</div>
        <div className={styles.field}>{'Скилы(EN): ' + item.skills_en}</div>
        <div className={styles.field}>{'Роль в группе: ' + item.group}</div>
        <div className={styles.field}>{'Порядковый номер: ' + item.serialNumber}</div>
        <div className={styles.field}>{'isExist: ' + item.isExist}</div>
        <Collapse>
          <Collapse.Panel header="Стек" key="1">
            <div className={styles.tags}>
              <div className={styles.row}>
                <div>
                  Стек:
                  <div>
                    <Button className={styles.button} type="primary" onClick={() => vm.openAddStackModal(item)}>
                      Добавить стек
                    </Button>
                  </div>
                </div>
              </div>
              {item.stackList?.map((stack) => {
                return (
                  <BaseCard
                    key={stack.id}
                    title={stack.title_ru}
                    extra={
                      <Space size="small" direction="horizontal">
                        <EditButton onClick={() => onEditStack(stack)} />
                        <DeleteButton onClick={() => onDeleteStack(stack.id)} />
                      </Space>
                    }
                  >
                    <div className={styles.field}>{'Описание(RU): ' + stack.description_ru}</div>
                    <div className={styles.field}>{'Описание(EN): ' + stack.description_en}</div>
                    <div className={styles.field}>{'Позиция: ' + stack.MemberStack?.serialNumber}</div>
                    <Image width={200} src={stack.image.url} />
                  </BaseCard>
                );
              })}
            </div>
          </Collapse.Panel>
          <Collapse.Panel header="Контакты" key="2">
            <div className={styles.tags}>
              <div className={styles.row}>
                <div>
                  Контакты:
                  <div>
                    <Button className={styles.button} type="primary" onClick={() => vm.openAddContactModal(item)}>
                      Создать контакт
                    </Button>
                  </div>
                </div>
              </div>
              {item.contactList?.map((contact) => {
                return (
                  <BaseCard
                    key={contact.id}
                    extra={
                      <Space size="small" direction="horizontal">
                        <EditButton onClick={() => onEditContact(contact)} />
                        <DeleteButton onClick={() => onDeleteContact(contact.id)} />
                      </Space>
                    }
                  >
                    <div className={styles.row}>
                      <div>
                        <div className={styles.field}>{'Какой мессенджер: ' + contact.type}</div>
                        <div className={styles.field}>{'Телефон/ссылка: ' + contact.value}</div>
                        <div className={styles.field}>{'Позиция: ' + contact.serialNumber}</div>
                      </div>
                    </div>
                  </BaseCard>
                );
              })}
            </div>
          </Collapse.Panel>
        </Collapse>
        <div className={styles.row}>
          <div>
            <div className={styles.field}>{'Картинка на превью: '}</div>
            <Image width={200} src={item.previewImage?.url} />
          </div>

          <div>
            <div className={styles.field}>{'Картинка фулл сайз: '}</div>
            <Image width={200} src={item.fullImage?.url} />
          </div>
        </div>
      </BaseCard>
    </>
  );
};

export const MemberListItem = observer(View);
