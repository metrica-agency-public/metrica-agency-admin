export const isValidPrice = (input: string | number): boolean => {
  let price: number;
  switch (typeof input) {
    case 'string':
      price = input === '' ? 0 : parseInt(input);
      break
    case 'number':
      price = input;
      break;
  }

  return !isNaN(price) && isFinite(price) && price > 0;
}
