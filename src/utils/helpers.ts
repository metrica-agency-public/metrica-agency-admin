import { FilterExistShowTypeRUS } from "./constants";

export const isSafari = () =>
  navigator.userAgent.toLowerCase().indexOf('safari/') !== -1 &&
  navigator.userAgent.toLowerCase().indexOf('chrome/') === -1;

export const makeDate = () => {
  const formatter = new Intl.DateTimeFormat('ru', {
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
  });
  return formatter.format(new Date());
}

export const formatShowTypeRUS = (showTypeEN: string) => {
  let rusKeyValue = Object.entries(FilterExistShowTypeRUS).find(el => el[0] === showTypeEN);
  if (rusKeyValue) {
    return rusKeyValue[1];
  }
  return FilterExistShowTypeRUS.ALL;
}