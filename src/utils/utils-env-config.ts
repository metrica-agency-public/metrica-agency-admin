import { plainToClass } from "class-transformer";

class ProcessENV {
  //Всё что касается основной БД
  public REACT_APP_BACK_URL: string = "https://....";
}

export default class UtilsENVConfig {
  static getProcessEnv(): ProcessENV {
    return plainToClass(ProcessENV, process.env);
  }
}
