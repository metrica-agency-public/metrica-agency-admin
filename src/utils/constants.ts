import UtilsENVConfig from "./utils-env-config";

export enum FilterExistShowType {
  ALL = 'ALL',
  DELETED = 'DELETED',
  EXISTING = 'EXISTING',
}

export enum FilterExistShowTypeRUS {
  ALL = 'ВСЕ',
  DELETED = 'Удаленные',
  EXISTING = 'Существующие',
}

export enum MemberContactType {
  HH = 'HH',
  TELEGRAM = 'TELEGRAM',
  WHATSAPP = 'WHATSAPP',
  PHONE = 'PHONE',
  GIT = 'GIT'
}

export enum ProjectPropertyImagePosition {
  LEFT = 'LEFT',
  RIGHT = 'RIGHT',
  UP = 'UP',
  DOWN = 'DOWN'
}

export enum ProjectPropertyImagePositionRUS {
  LEFT = 'Слева',
  RIGHT = 'Справа',
  UP = 'Сверху',
  DOWN = 'Снизу'
}

export enum MemberGroup {
  DEVELOPER = 'DEVELOPER',
  MAINTAINER = 'MAINTAINER',
  DESIGNER = 'DESIGNER',
  ANALYTICS_AND_QA = 'ANALYTICS_AND_QA',
}

export class Constants {
  public static API_V1_URL: string = `${UtilsENVConfig.getProcessEnv().REACT_APP_BACK_URL}/api/v1`;
}
